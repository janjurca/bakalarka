import sys

print('''
#!/bin/bash
module add tensorflow-1.7.1-cpu-python3
module add cmake-3.6.1
pip3 install sphinx==1.3 --user
module add opencv-3.3.1-py34
pip3 install dlib --user

cd $SCRATCHDIR || exit 1
git clone https://gitlab.com/kohoutovice/bakalarka.git
cp /storage/brno2/home/xjurca08/frame_images_DB.tar.gz ./

tar -xvzf frame_images_DB.tar.gz
''')







all = 621126
cpus = int(sys.argv[1])

interval = all / cpus

for i in range(cpus):
    print('''python3 bakalarka/facenet/Facial-Recognition-using-Facenet/rec_feat.py --file facenet.out'''+str(int(i))+'''.pic -v $SCRATCHDIR/ --start '''+str(int(i*interval))+''' --stop '''+str(int((i+1)*interval-1))+''' &''')
    print('''p1=$!''')


for i in range(cpus):
    print("wait $p"+str(i))
    print("cp facenet.out"+str(i)+".pic /storage/brno2/home/xjurca08/descs/facenet || export CLEAN_SCRATCH=false")
