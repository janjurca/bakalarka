import sys

print('''#!/bin/bash
cd $SCRATCHDIR || exit 1
mkdir temp
TMPDIR=$(pwd)/temp
export TMPDIR
module add python36-modules-gcc
pip3 install  torch torchvision  --cache-dir $(pwd)/cache --root $(pwd)

PYTHONPATH=$(pwd)/software/python-3.6.2/gcc/lib/python3.6/site-packages/:$PYTHONPATH

cp /storage/brno2/home/xjurca08/DukeMTMC-VideoReID.zip ./
unzip DukeMTMC-VideoReID.zip

git clone https://gitlab.com/kohoutovice/bakalarka.git
cd bakalarka/person/deep-person-reid/

''')



all = 927268
cpus = int(sys.argv[1])

interval = all / cpus

for i in range(cpus):
    print('''python3 eval.py --load-weights resnet50_market_xent.pth -a resnet50 --folder ../../../DukeMTMC-VideoReID/ --file person.out'''+str(int(i))+'''.pic --start '''+str(int(i*interval))+''' --stop '''+str(int((i+1)*interval-1))+''' > /dev/null  &''')
    print('''p'''+str(i)+'''=$!''')


for i in range(cpus):
    print("wait $p"+str(i))
    print("cp person.out"+str(i)+".pic /storage/brno2/home/xjurca08/descs/person || export CLEAN_SCRATCH=false")
