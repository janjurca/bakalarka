#!/bin/bash
cd $SCRATCHDIR || exit 1
mkdir temp
TMPDIR=$(pwd)/temp
export TMPDIR
module add python36-modules-gcc
pip3 install  torch torchvision  --cache-dir $(pwd)/cache --root $(pwd)

PYTHONPATH=$(pwd)/software/python-3.6.2/gcc/lib/python3.6/site-packages/:$PYTHONPATH

cp /storage/brno2/home/xjurca08/DukeMTMC-VideoReID.zip ./
unzip DukeMTMC-VideoReID.zip

git clone https://gitlab.com/kohoutovice/bakalarka.git
cd bakalarka/person/deep-person-reid/


python3 eval.py --load-weights resnet50_market_xent.pth -a resnet50 --folder ../../../DukeMTMC-VideoReID/ --file person.out0.pic --start 0 --stop 28976 > /dev/null  &
p0=$!
python3 eval.py --load-weights resnet50_market_xent.pth -a resnet50 --folder ../../../DukeMTMC-VideoReID/ --file person.out1.pic --start 28977 --stop 57953 > /dev/null  &
p1=$!
python3 eval.py --load-weights resnet50_market_xent.pth -a resnet50 --folder ../../../DukeMTMC-VideoReID/ --file person.out2.pic --start 57954 --stop 86930 > /dev/null  &
p2=$!
python3 eval.py --load-weights resnet50_market_xent.pth -a resnet50 --folder ../../../DukeMTMC-VideoReID/ --file person.out3.pic --start 86931 --stop 115907 > /dev/null  &
p3=$!
python3 eval.py --load-weights resnet50_market_xent.pth -a resnet50 --folder ../../../DukeMTMC-VideoReID/ --file person.out4.pic --start 115908 --stop 144884 > /dev/null  &
p4=$!
python3 eval.py --load-weights resnet50_market_xent.pth -a resnet50 --folder ../../../DukeMTMC-VideoReID/ --file person.out5.pic --start 144885 --stop 173861 > /dev/null  &
p5=$!
python3 eval.py --load-weights resnet50_market_xent.pth -a resnet50 --folder ../../../DukeMTMC-VideoReID/ --file person.out6.pic --start 173862 --stop 202838 > /dev/null  &
p6=$!
python3 eval.py --load-weights resnet50_market_xent.pth -a resnet50 --folder ../../../DukeMTMC-VideoReID/ --file person.out7.pic --start 202839 --stop 231816 > /dev/null  &
p7=$!
python3 eval.py --load-weights resnet50_market_xent.pth -a resnet50 --folder ../../../DukeMTMC-VideoReID/ --file person.out8.pic --start 231817 --stop 260793 > /dev/null  &
p8=$!
python3 eval.py --load-weights resnet50_market_xent.pth -a resnet50 --folder ../../../DukeMTMC-VideoReID/ --file person.out9.pic --start 260794 --stop 289770 > /dev/null  &
p9=$!
python3 eval.py --load-weights resnet50_market_xent.pth -a resnet50 --folder ../../../DukeMTMC-VideoReID/ --file person.out10.pic --start 289771 --stop 318747 > /dev/null  &
p10=$!
python3 eval.py --load-weights resnet50_market_xent.pth -a resnet50 --folder ../../../DukeMTMC-VideoReID/ --file person.out11.pic --start 318748 --stop 347724 > /dev/null  &
p11=$!
python3 eval.py --load-weights resnet50_market_xent.pth -a resnet50 --folder ../../../DukeMTMC-VideoReID/ --file person.out12.pic --start 347725 --stop 376701 > /dev/null  &
p12=$!
python3 eval.py --load-weights resnet50_market_xent.pth -a resnet50 --folder ../../../DukeMTMC-VideoReID/ --file person.out13.pic --start 376702 --stop 405678 > /dev/null  &
p13=$!
python3 eval.py --load-weights resnet50_market_xent.pth -a resnet50 --folder ../../../DukeMTMC-VideoReID/ --file person.out14.pic --start 405679 --stop 434655 > /dev/null  &
p14=$!
python3 eval.py --load-weights resnet50_market_xent.pth -a resnet50 --folder ../../../DukeMTMC-VideoReID/ --file person.out15.pic --start 434656 --stop 463633 > /dev/null  &
p15=$!
python3 eval.py --load-weights resnet50_market_xent.pth -a resnet50 --folder ../../../DukeMTMC-VideoReID/ --file person.out16.pic --start 463634 --stop 492610 > /dev/null  &
p16=$!
python3 eval.py --load-weights resnet50_market_xent.pth -a resnet50 --folder ../../../DukeMTMC-VideoReID/ --file person.out17.pic --start 492611 --stop 521587 > /dev/null  &
p17=$!
python3 eval.py --load-weights resnet50_market_xent.pth -a resnet50 --folder ../../../DukeMTMC-VideoReID/ --file person.out18.pic --start 521588 --stop 550564 > /dev/null  &
p18=$!
python3 eval.py --load-weights resnet50_market_xent.pth -a resnet50 --folder ../../../DukeMTMC-VideoReID/ --file person.out19.pic --start 550565 --stop 579541 > /dev/null  &
p19=$!
python3 eval.py --load-weights resnet50_market_xent.pth -a resnet50 --folder ../../../DukeMTMC-VideoReID/ --file person.out20.pic --start 579542 --stop 608518 > /dev/null  &
p20=$!
python3 eval.py --load-weights resnet50_market_xent.pth -a resnet50 --folder ../../../DukeMTMC-VideoReID/ --file person.out21.pic --start 608519 --stop 637495 > /dev/null  &
p21=$!
python3 eval.py --load-weights resnet50_market_xent.pth -a resnet50 --folder ../../../DukeMTMC-VideoReID/ --file person.out22.pic --start 637496 --stop 666472 > /dev/null  &
p22=$!
python3 eval.py --load-weights resnet50_market_xent.pth -a resnet50 --folder ../../../DukeMTMC-VideoReID/ --file person.out23.pic --start 666473 --stop 695450 > /dev/null  &
p23=$!
python3 eval.py --load-weights resnet50_market_xent.pth -a resnet50 --folder ../../../DukeMTMC-VideoReID/ --file person.out24.pic --start 695451 --stop 724427 > /dev/null  &
p24=$!
python3 eval.py --load-weights resnet50_market_xent.pth -a resnet50 --folder ../../../DukeMTMC-VideoReID/ --file person.out25.pic --start 724428 --stop 753404 > /dev/null  &
p25=$!
python3 eval.py --load-weights resnet50_market_xent.pth -a resnet50 --folder ../../../DukeMTMC-VideoReID/ --file person.out26.pic --start 753405 --stop 782381 > /dev/null  &
p26=$!
python3 eval.py --load-weights resnet50_market_xent.pth -a resnet50 --folder ../../../DukeMTMC-VideoReID/ --file person.out27.pic --start 782382 --stop 811358 > /dev/null  &
p27=$!
python3 eval.py --load-weights resnet50_market_xent.pth -a resnet50 --folder ../../../DukeMTMC-VideoReID/ --file person.out28.pic --start 811359 --stop 840335 > /dev/null  &
p28=$!
python3 eval.py --load-weights resnet50_market_xent.pth -a resnet50 --folder ../../../DukeMTMC-VideoReID/ --file person.out29.pic --start 840336 --stop 869312 > /dev/null  &
p29=$!
python3 eval.py --load-weights resnet50_market_xent.pth -a resnet50 --folder ../../../DukeMTMC-VideoReID/ --file person.out30.pic --start 869313 --stop 898289 > /dev/null  &
p30=$!
python3 eval.py --load-weights resnet50_market_xent.pth -a resnet50 --folder ../../../DukeMTMC-VideoReID/ --file person.out31.pic --start 898290 --stop 927267 > /dev/null  &
p31=$!
wait $p0
cp person.out0.pic /storage/brno2/home/xjurca08/descs/person || export CLEAN_SCRATCH=false
wait $p1
cp person.out1.pic /storage/brno2/home/xjurca08/descs/person || export CLEAN_SCRATCH=false
wait $p2
cp person.out2.pic /storage/brno2/home/xjurca08/descs/person || export CLEAN_SCRATCH=false
wait $p3
cp person.out3.pic /storage/brno2/home/xjurca08/descs/person || export CLEAN_SCRATCH=false
wait $p4
cp person.out4.pic /storage/brno2/home/xjurca08/descs/person || export CLEAN_SCRATCH=false
wait $p5
cp person.out5.pic /storage/brno2/home/xjurca08/descs/person || export CLEAN_SCRATCH=false
wait $p6
cp person.out6.pic /storage/brno2/home/xjurca08/descs/person || export CLEAN_SCRATCH=false
wait $p7
cp person.out7.pic /storage/brno2/home/xjurca08/descs/person || export CLEAN_SCRATCH=false
wait $p8
cp person.out8.pic /storage/brno2/home/xjurca08/descs/person || export CLEAN_SCRATCH=false
wait $p9
cp person.out9.pic /storage/brno2/home/xjurca08/descs/person || export CLEAN_SCRATCH=false
wait $p10
cp person.out10.pic /storage/brno2/home/xjurca08/descs/person || export CLEAN_SCRATCH=false
wait $p11
cp person.out11.pic /storage/brno2/home/xjurca08/descs/person || export CLEAN_SCRATCH=false
wait $p12
cp person.out12.pic /storage/brno2/home/xjurca08/descs/person || export CLEAN_SCRATCH=false
wait $p13
cp person.out13.pic /storage/brno2/home/xjurca08/descs/person || export CLEAN_SCRATCH=false
wait $p14
cp person.out14.pic /storage/brno2/home/xjurca08/descs/person || export CLEAN_SCRATCH=false
wait $p15
cp person.out15.pic /storage/brno2/home/xjurca08/descs/person || export CLEAN_SCRATCH=false
wait $p16
cp person.out16.pic /storage/brno2/home/xjurca08/descs/person || export CLEAN_SCRATCH=false
wait $p17
cp person.out17.pic /storage/brno2/home/xjurca08/descs/person || export CLEAN_SCRATCH=false
wait $p18
cp person.out18.pic /storage/brno2/home/xjurca08/descs/person || export CLEAN_SCRATCH=false
wait $p19
cp person.out19.pic /storage/brno2/home/xjurca08/descs/person || export CLEAN_SCRATCH=false
wait $p20
cp person.out20.pic /storage/brno2/home/xjurca08/descs/person || export CLEAN_SCRATCH=false
wait $p21
cp person.out21.pic /storage/brno2/home/xjurca08/descs/person || export CLEAN_SCRATCH=false
wait $p22
cp person.out22.pic /storage/brno2/home/xjurca08/descs/person || export CLEAN_SCRATCH=false
wait $p23
cp person.out23.pic /storage/brno2/home/xjurca08/descs/person || export CLEAN_SCRATCH=false
wait $p24
cp person.out24.pic /storage/brno2/home/xjurca08/descs/person || export CLEAN_SCRATCH=false
wait $p25
cp person.out25.pic /storage/brno2/home/xjurca08/descs/person || export CLEAN_SCRATCH=false
wait $p26
cp person.out26.pic /storage/brno2/home/xjurca08/descs/person || export CLEAN_SCRATCH=false
wait $p27
cp person.out27.pic /storage/brno2/home/xjurca08/descs/person || export CLEAN_SCRATCH=false
wait $p28
cp person.out28.pic /storage/brno2/home/xjurca08/descs/person || export CLEAN_SCRATCH=false
wait $p29
cp person.out29.pic /storage/brno2/home/xjurca08/descs/person || export CLEAN_SCRATCH=false
wait $p30
cp person.out30.pic /storage/brno2/home/xjurca08/descs/person || export CLEAN_SCRATCH=false
wait $p31
cp person.out31.pic /storage/brno2/home/xjurca08/descs/person || export CLEAN_SCRATCH=false
