#!/bin/bash

module add pytorch-0.3.0_python-3.6.2_cpu

export CLEAN_SCRATCH=false

cd $SCRATCHDIR || exit 1

git clone https://gitlab.com/kohoutovice/bakalarka.git

cp /storage/brno2/home/xjurca08/dataset.triplet.fixed.pic ./



python3 bakalarka/fusion_test/train.py --src dataset.triplet.fixed.pic
