#!/bin/bash


module add tensorflow-1.7.1-cpu-python3

cd $SCRATCHDIR || exit 1
git clone https://gitlab.com/kohoutovice/bakalarka.git
cp /storage/brno2/home/xjurca08/aligned_images_DB.tar.gz ./

tar -xvzf aligned_images_DB.tar.gz

cp /storage/brno2/home/xjurca08/vgg_face_weights.h5 ./

python3 bakalarka/VGG/extract.py --file out7.pic -v aligned_images_DB/ -m vgg_face_weights.h5 --start 227769 --stop 265729 &
p1=$!
python3 bakalarka/VGG/extract.py --file out8.pic -v aligned_images_DB/ -m vgg_face_weights.h5 --start 265730 --stop 303694 &
p2=$!
python3 bakalarka/VGG/extract.py --file out9.pic -v aligned_images_DB/ -m vgg_face_weights.h5 --start 303695 --stop 341654 &
p3=$!
python3 bakalarka/VGG/extract.py --file out10.pic -v aligned_images_DB/ -m vgg_face_weights.h5 --start 341655 --stop 379614 &
p4=$!
python3 bakalarka/VGG/extract.py --file out11.pic -v aligned_images_DB/ -m vgg_face_weights.h5 --start 379615 --stop 417574 &
p5=$!
python3 bakalarka/VGG/extract.py --file out12.pic -v aligned_images_DB/ -m vgg_face_weights.h5 --start 417575 --stop 455534 &
p6=$!
python3 bakalarka/VGG/extract.py --file out13.pic -v aligned_images_DB/ -m vgg_face_weights.h5 --start 455535 --stop 493494 &
p7=$!
python3 bakalarka/VGG/extract.py --file out14.pic -v aligned_images_DB/ -m vgg_face_weights.h5 --start 493495 --stop 531454 &
p8=$!
python3 bakalarka/VGG/extract.py --file out15.pic -v aligned_images_DB/ -m vgg_face_weights.h5 --start 531455 --stop 569414 &
p9=$!
python3 bakalarka/VGG/extract.py --file out16.pic -v aligned_images_DB/ -m vgg_face_weights.h5 --start 569415 --stop 607389 &
p10=$!

wait $p1
wait $p2
wait $p3
wait $p4
wait $p5
wait $p6
wait $p7
wait $p8
wait $p9
wait $p10

cp out7.pic /storage/brno2/home/xjurca08/ || export CLEAN_SCRATCH=false
cp out8.pic /storage/brno2/home/xjurca08/ || export CLEAN_SCRATCH=false
cp out9.pic /storage/brno2/home/xjurca08/ || export CLEAN_SCRATCH=false
cp out10.pic /storage/brno2/home/xjurca08/ || export CLEAN_SCRATCH=false
cp out11.pic /storage/brno2/home/xjurca08/ || export CLEAN_SCRATCH=false
cp out12.pic /storage/brno2/home/xjurca08/ || export CLEAN_SCRATCH=false
cp out13.pic /storage/brno2/home/xjurca08/ || export CLEAN_SCRATCH=false
cp out14.pic /storage/brno2/home/xjurca08/ || export CLEAN_SCRATCH=false
cp out15.pic /storage/brno2/home/xjurca08/ || export CLEAN_SCRATCH=false
cp out16.pic /storage/brno2/home/xjurca08/ || export CLEAN_SCRATCH=false
