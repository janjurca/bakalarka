#!/usr/bin/python3
import sys

print('''#!/bin/bash
cd $SCRATCHDIR || exit 1
module add tensorflow-1.7.1-gpu-python3
git clone https://gitlab.com/kohoutovice/bakalarka.git
cd bakalarka/gait/tf-openpose/
module add swig-3.0.8
cd tf_pose/pafprocess/
swig -python -c++ pafprocess.i && python3 setup.py build_ext --inplace
cd ../..

pip3 install opencv-python --user
pip3 install slidingwindow --user
cd models/graph/cmu/
bash download.sh
cd ../../..
module rm python-3.4.1-gcc python34-modules-gcc
module add python34-modules-intel
''')
file = sys.argv[1]

print("python3 run_eval_video.py --video " +sys.argv[1] +" --resize 1312x736 --file "+sys.argv[1]+".pose")
