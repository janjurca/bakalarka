cd $SCRATCHDIR || exit 1
git clone https://github.com/davisking/dlib.git
cd dlib
module add python34-modules-gcc
module add cmake-3.6.1
module add cuda-9.0
module add cudnn-7.0
CMAKE_PREFIX_PATH=$CMAKE_PREFIX_PATH:/software/cudnn/7.0/
export CMAKE_PREFIX_PATH
PYTHONPATH=$PYTHONPATH:$(pwd)/lib/python3.4/site-packages/
mkdir -p $(pwd)/lib/python3.4/site-packages/
python3 setup.py install --prefix=$(pwd)



git clone https://gitlab.com/kohoutovice/bakalarka.git
cd bakalarka/face/dlib

mkdir dataset
cd dataset
cp /storage/brno8/home/xjurca08/bbox_train.zip ./
cp /storage/brno8/home/xjurca08/bbox_test.zip ./
unzip bbox_test.zip
unzip bbox_train.zip
cd ..

python3.4 face_recognition_cuda.py --file /storage/budejovice1/home/xjurca08/BUT_pedestrians_face_descs.pic  --glob './BUT_pedestrians/*/*/*/*jpg'
