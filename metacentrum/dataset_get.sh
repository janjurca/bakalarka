#!/bin/bash

# nastaveni domovskeho adresare, v promenne $LOGNAME je ulozeno vase prihlasovaci jmeno
DATADIR="/storage/brno2/home/$LOGNAME/"

# nastaveni automatickeho vymazani adresare SCRATCH pro pripad chyby pri behu ulohy
trap 'clean_scratch' TERM EXIT

# vstup do adresare SCRATCH, nebo v pripade neuspechu ukonceni s chybovou hodnotou rovnou 1
cd $SCRATCHDIR || exit 1

wget http://www.cslab.openu.ac.il/download/wolftau/aligned_images_DB.tar.gz --user wolftau --password wtal997 -o $DATADIR/dataset/aligned_images_DB.tar.gz


# kopirovani vystupnich dat z vypocetnicho uzlu do domovskeho adresare,
# pokud by pri kopirovani doslo k chybe, nebude adresar SCRATCH vymazan pro moznost rucniho vyzvednuti dat
#cp -R aligned_images_DB  $DATADIR/dataset || export CLEAN_SCRATCH=false
