import sys

print('''#!/bin/bash
module add tensorflow-1.7.1-cpu-python3
module add cmake-3.6.1
pip3 install sphinx==1.3 --user
pip3 install dlib --user
pip3 install opencv-python  --user
cd $SCRATCHDIR || exit 1
git clone https://gitlab.com/kohoutovice/bakalarka.git
cp /storage/brno2/home/xjurca08/BUT_pedestrians.zip ./

unzip BUT_pedestrians.zip

''')



all = 250840
cpus = int(sys.argv[1])

interval = all / cpus

for i in range(cpus):
    print('''python3 bakalarka/dlib_extract/face_recognition.py -p bakalarka/dlib_extract/shape_predictor_5_face_landmarks.dat -m bakalarka/dlib_extract/dlib_face_recognition_resnet_model_v1.dat --file dlib.out'''+str(int(i))+'''.pic -v $SCRATCHDIR/BUT_pedestrians --start '''+str(int(i*interval))+''' --stop '''+str(int((i+1)*interval-1))+'''  &''')
    print('''p'''+str(i)+'''=$!''')


for i in range(cpus):
    print("wait $p"+str(i))
    print("cp dlib.out"+str(i)+".pic /storage/brno2/home/xjurca08/descs/BUT_pedestrians/dlib || export CLEAN_SCRATCH=false")
