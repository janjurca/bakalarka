
#!/bin/bash
module add tensorflow-1.7.1-cpu-python3
module add cmake-3.6.1
pip3 install sphinx==1.3 --user
module add opencv-3.3.1-py34
pip3 install dlib --user

cd $SCRATCHDIR || exit 1
git clone https://gitlab.com/kohoutovice/bakalarka.git
cp /storage/brno2/home/xjurca08/frame_images_DB.tar.gz ./

tar -xvzf frame_images_DB.tar.gz

python3 bakalarka/dlib_extract/face_recognition.py -p bakalarka/dlib_extract/shape_predictor_5_face_landmarks.dat -m bakalarka/dlib_extract/dlib_face_recognition_resnet_model_v1.dat --file dlib.out0.pic -v $SCRATCHDIR/ --start 0 --stop 19409  &
p1=$!
python3 bakalarka/dlib_extract/face_recognition.py -p bakalarka/dlib_extract/shape_predictor_5_face_landmarks.dat -m bakalarka/dlib_extract/dlib_face_recognition_resnet_model_v1.dat --file dlib.out1.pic -v $SCRATCHDIR/ --start 19410 --stop 38819  &
p1=$!
python3 bakalarka/dlib_extract/face_recognition.py -p bakalarka/dlib_extract/shape_predictor_5_face_landmarks.dat -m bakalarka/dlib_extract/dlib_face_recognition_resnet_model_v1.dat --file dlib.out2.pic -v $SCRATCHDIR/ --start 38820 --stop 58229  &
p1=$!
python3 bakalarka/dlib_extract/face_recognition.py -p bakalarka/dlib_extract/shape_predictor_5_face_landmarks.dat -m bakalarka/dlib_extract/dlib_face_recognition_resnet_model_v1.dat --file dlib.out3.pic -v $SCRATCHDIR/ --start 58230 --stop 77639  &
p1=$!
python3 bakalarka/dlib_extract/face_recognition.py -p bakalarka/dlib_extract/shape_predictor_5_face_landmarks.dat -m bakalarka/dlib_extract/dlib_face_recognition_resnet_model_v1.dat --file dlib.out4.pic -v $SCRATCHDIR/ --start 77640 --stop 97049  &
p1=$!
python3 bakalarka/dlib_extract/face_recognition.py -p bakalarka/dlib_extract/shape_predictor_5_face_landmarks.dat -m bakalarka/dlib_extract/dlib_face_recognition_resnet_model_v1.dat --file dlib.out5.pic -v $SCRATCHDIR/ --start 97050 --stop 116460  &
p1=$!
python3 bakalarka/dlib_extract/face_recognition.py -p bakalarka/dlib_extract/shape_predictor_5_face_landmarks.dat -m bakalarka/dlib_extract/dlib_face_recognition_resnet_model_v1.dat --file dlib.out6.pic -v $SCRATCHDIR/ --start 116461 --stop 135870  &
p1=$!
python3 bakalarka/dlib_extract/face_recognition.py -p bakalarka/dlib_extract/shape_predictor_5_face_landmarks.dat -m bakalarka/dlib_extract/dlib_face_recognition_resnet_model_v1.dat --file dlib.out7.pic -v $SCRATCHDIR/ --start 135871 --stop 155280  &
p1=$!
python3 bakalarka/dlib_extract/face_recognition.py -p bakalarka/dlib_extract/shape_predictor_5_face_landmarks.dat -m bakalarka/dlib_extract/dlib_face_recognition_resnet_model_v1.dat --file dlib.out8.pic -v $SCRATCHDIR/ --start 155281 --stop 174690  &
p1=$!
python3 bakalarka/dlib_extract/face_recognition.py -p bakalarka/dlib_extract/shape_predictor_5_face_landmarks.dat -m bakalarka/dlib_extract/dlib_face_recognition_resnet_model_v1.dat --file dlib.out9.pic -v $SCRATCHDIR/ --start 174691 --stop 194100  &
p1=$!
python3 bakalarka/dlib_extract/face_recognition.py -p bakalarka/dlib_extract/shape_predictor_5_face_landmarks.dat -m bakalarka/dlib_extract/dlib_face_recognition_resnet_model_v1.dat --file dlib.out10.pic -v $SCRATCHDIR/ --start 194101 --stop 213511  &
p1=$!
python3 bakalarka/dlib_extract/face_recognition.py -p bakalarka/dlib_extract/shape_predictor_5_face_landmarks.dat -m bakalarka/dlib_extract/dlib_face_recognition_resnet_model_v1.dat --file dlib.out11.pic -v $SCRATCHDIR/ --start 213512 --stop 232921  &
p1=$!
python3 bakalarka/dlib_extract/face_recognition.py -p bakalarka/dlib_extract/shape_predictor_5_face_landmarks.dat -m bakalarka/dlib_extract/dlib_face_recognition_resnet_model_v1.dat --file dlib.out12.pic -v $SCRATCHDIR/ --start 232922 --stop 252331  &
p1=$!
python3 bakalarka/dlib_extract/face_recognition.py -p bakalarka/dlib_extract/shape_predictor_5_face_landmarks.dat -m bakalarka/dlib_extract/dlib_face_recognition_resnet_model_v1.dat --file dlib.out13.pic -v $SCRATCHDIR/ --start 252332 --stop 271741  &
p1=$!
python3 bakalarka/dlib_extract/face_recognition.py -p bakalarka/dlib_extract/shape_predictor_5_face_landmarks.dat -m bakalarka/dlib_extract/dlib_face_recognition_resnet_model_v1.dat --file dlib.out14.pic -v $SCRATCHDIR/ --start 271742 --stop 291151  &
p1=$!
python3 bakalarka/dlib_extract/face_recognition.py -p bakalarka/dlib_extract/shape_predictor_5_face_landmarks.dat -m bakalarka/dlib_extract/dlib_face_recognition_resnet_model_v1.dat --file dlib.out15.pic -v $SCRATCHDIR/ --start 291152 --stop 310562  &
p1=$!
python3 bakalarka/dlib_extract/face_recognition.py -p bakalarka/dlib_extract/shape_predictor_5_face_landmarks.dat -m bakalarka/dlib_extract/dlib_face_recognition_resnet_model_v1.dat --file dlib.out16.pic -v $SCRATCHDIR/ --start 310563 --stop 329972  &
p1=$!
python3 bakalarka/dlib_extract/face_recognition.py -p bakalarka/dlib_extract/shape_predictor_5_face_landmarks.dat -m bakalarka/dlib_extract/dlib_face_recognition_resnet_model_v1.dat --file dlib.out17.pic -v $SCRATCHDIR/ --start 329973 --stop 349382  &
p1=$!
python3 bakalarka/dlib_extract/face_recognition.py -p bakalarka/dlib_extract/shape_predictor_5_face_landmarks.dat -m bakalarka/dlib_extract/dlib_face_recognition_resnet_model_v1.dat --file dlib.out18.pic -v $SCRATCHDIR/ --start 349383 --stop 368792  &
p1=$!
python3 bakalarka/dlib_extract/face_recognition.py -p bakalarka/dlib_extract/shape_predictor_5_face_landmarks.dat -m bakalarka/dlib_extract/dlib_face_recognition_resnet_model_v1.dat --file dlib.out19.pic -v $SCRATCHDIR/ --start 368793 --stop 388202  &
p1=$!
python3 bakalarka/dlib_extract/face_recognition.py -p bakalarka/dlib_extract/shape_predictor_5_face_landmarks.dat -m bakalarka/dlib_extract/dlib_face_recognition_resnet_model_v1.dat --file dlib.out20.pic -v $SCRATCHDIR/ --start 388203 --stop 407612  &
p1=$!
python3 bakalarka/dlib_extract/face_recognition.py -p bakalarka/dlib_extract/shape_predictor_5_face_landmarks.dat -m bakalarka/dlib_extract/dlib_face_recognition_resnet_model_v1.dat --file dlib.out21.pic -v $SCRATCHDIR/ --start 407613 --stop 427023  &
p1=$!
python3 bakalarka/dlib_extract/face_recognition.py -p bakalarka/dlib_extract/shape_predictor_5_face_landmarks.dat -m bakalarka/dlib_extract/dlib_face_recognition_resnet_model_v1.dat --file dlib.out22.pic -v $SCRATCHDIR/ --start 427024 --stop 446433  &
p1=$!
python3 bakalarka/dlib_extract/face_recognition.py -p bakalarka/dlib_extract/shape_predictor_5_face_landmarks.dat -m bakalarka/dlib_extract/dlib_face_recognition_resnet_model_v1.dat --file dlib.out23.pic -v $SCRATCHDIR/ --start 446434 --stop 465843  &
p1=$!
python3 bakalarka/dlib_extract/face_recognition.py -p bakalarka/dlib_extract/shape_predictor_5_face_landmarks.dat -m bakalarka/dlib_extract/dlib_face_recognition_resnet_model_v1.dat --file dlib.out24.pic -v $SCRATCHDIR/ --start 465844 --stop 485253  &
p1=$!
python3 bakalarka/dlib_extract/face_recognition.py -p bakalarka/dlib_extract/shape_predictor_5_face_landmarks.dat -m bakalarka/dlib_extract/dlib_face_recognition_resnet_model_v1.dat --file dlib.out25.pic -v $SCRATCHDIR/ --start 485254 --stop 504663  &
p1=$!
python3 bakalarka/dlib_extract/face_recognition.py -p bakalarka/dlib_extract/shape_predictor_5_face_landmarks.dat -m bakalarka/dlib_extract/dlib_face_recognition_resnet_model_v1.dat --file dlib.out26.pic -v $SCRATCHDIR/ --start 504664 --stop 524074  &
p1=$!
python3 bakalarka/dlib_extract/face_recognition.py -p bakalarka/dlib_extract/shape_predictor_5_face_landmarks.dat -m bakalarka/dlib_extract/dlib_face_recognition_resnet_model_v1.dat --file dlib.out27.pic -v $SCRATCHDIR/ --start 524075 --stop 543484  &
p1=$!
python3 bakalarka/dlib_extract/face_recognition.py -p bakalarka/dlib_extract/shape_predictor_5_face_landmarks.dat -m bakalarka/dlib_extract/dlib_face_recognition_resnet_model_v1.dat --file dlib.out28.pic -v $SCRATCHDIR/ --start 543485 --stop 562894  &
p1=$!
python3 bakalarka/dlib_extract/face_recognition.py -p bakalarka/dlib_extract/shape_predictor_5_face_landmarks.dat -m bakalarka/dlib_extract/dlib_face_recognition_resnet_model_v1.dat --file dlib.out29.pic -v $SCRATCHDIR/ --start 562895 --stop 582304  &
p1=$!
python3 bakalarka/dlib_extract/face_recognition.py -p bakalarka/dlib_extract/shape_predictor_5_face_landmarks.dat -m bakalarka/dlib_extract/dlib_face_recognition_resnet_model_v1.dat --file dlib.out30.pic -v $SCRATCHDIR/ --start 582305 --stop 601714  &
p1=$!
python3 bakalarka/dlib_extract/face_recognition.py -p bakalarka/dlib_extract/shape_predictor_5_face_landmarks.dat -m bakalarka/dlib_extract/dlib_face_recognition_resnet_model_v1.dat --file dlib.out31.pic -v $SCRATCHDIR/ --start 601715 --stop 621125  &
p1=$!
wait $p0
cp dlib.out0.pic /storage/brno2/home/xjurca08/descs/dlib || export CLEAN_SCRATCH=false
wait $p1
cp dlib.out1.pic /storage/brno2/home/xjurca08/descs/dlib || export CLEAN_SCRATCH=false
wait $p2
cp dlib.out2.pic /storage/brno2/home/xjurca08/descs/dlib || export CLEAN_SCRATCH=false
wait $p3
cp dlib.out3.pic /storage/brno2/home/xjurca08/descs/dlib || export CLEAN_SCRATCH=false
wait $p4
cp dlib.out4.pic /storage/brno2/home/xjurca08/descs/dlib || export CLEAN_SCRATCH=false
wait $p5
cp dlib.out5.pic /storage/brno2/home/xjurca08/descs/dlib || export CLEAN_SCRATCH=false
wait $p6
cp dlib.out6.pic /storage/brno2/home/xjurca08/descs/dlib || export CLEAN_SCRATCH=false
wait $p7
cp dlib.out7.pic /storage/brno2/home/xjurca08/descs/dlib || export CLEAN_SCRATCH=false
wait $p8
cp dlib.out8.pic /storage/brno2/home/xjurca08/descs/dlib || export CLEAN_SCRATCH=false
wait $p9
cp dlib.out9.pic /storage/brno2/home/xjurca08/descs/dlib || export CLEAN_SCRATCH=false
wait $p10
cp dlib.out10.pic /storage/brno2/home/xjurca08/descs/dlib || export CLEAN_SCRATCH=false
wait $p11
cp dlib.out11.pic /storage/brno2/home/xjurca08/descs/dlib || export CLEAN_SCRATCH=false
wait $p12
cp dlib.out12.pic /storage/brno2/home/xjurca08/descs/dlib || export CLEAN_SCRATCH=false
wait $p13
cp dlib.out13.pic /storage/brno2/home/xjurca08/descs/dlib || export CLEAN_SCRATCH=false
wait $p14
cp dlib.out14.pic /storage/brno2/home/xjurca08/descs/dlib || export CLEAN_SCRATCH=false
wait $p15
cp dlib.out15.pic /storage/brno2/home/xjurca08/descs/dlib || export CLEAN_SCRATCH=false
wait $p16
cp dlib.out16.pic /storage/brno2/home/xjurca08/descs/dlib || export CLEAN_SCRATCH=false
wait $p17
cp dlib.out17.pic /storage/brno2/home/xjurca08/descs/dlib || export CLEAN_SCRATCH=false
wait $p18
cp dlib.out18.pic /storage/brno2/home/xjurca08/descs/dlib || export CLEAN_SCRATCH=false
wait $p19
cp dlib.out19.pic /storage/brno2/home/xjurca08/descs/dlib || export CLEAN_SCRATCH=false
wait $p20
cp dlib.out20.pic /storage/brno2/home/xjurca08/descs/dlib || export CLEAN_SCRATCH=false
wait $p21
cp dlib.out21.pic /storage/brno2/home/xjurca08/descs/dlib || export CLEAN_SCRATCH=false
wait $p22
cp dlib.out22.pic /storage/brno2/home/xjurca08/descs/dlib || export CLEAN_SCRATCH=false
wait $p23
cp dlib.out23.pic /storage/brno2/home/xjurca08/descs/dlib || export CLEAN_SCRATCH=false
wait $p24
cp dlib.out24.pic /storage/brno2/home/xjurca08/descs/dlib || export CLEAN_SCRATCH=false
wait $p25
cp dlib.out25.pic /storage/brno2/home/xjurca08/descs/dlib || export CLEAN_SCRATCH=false
wait $p26
cp dlib.out26.pic /storage/brno2/home/xjurca08/descs/dlib || export CLEAN_SCRATCH=false
wait $p27
cp dlib.out27.pic /storage/brno2/home/xjurca08/descs/dlib || export CLEAN_SCRATCH=false
wait $p28
cp dlib.out28.pic /storage/brno2/home/xjurca08/descs/dlib || export CLEAN_SCRATCH=false
wait $p29
cp dlib.out29.pic /storage/brno2/home/xjurca08/descs/dlib || export CLEAN_SCRATCH=false
wait $p30
cp dlib.out30.pic /storage/brno2/home/xjurca08/descs/dlib || export CLEAN_SCRATCH=false
wait $p31
cp dlib.out31.pic /storage/brno2/home/xjurca08/descs/dlib || export CLEAN_SCRATCH=false
