
#!/bin/bash
module add tensorflow-1.7.1-cpu-python3
module add cmake-3.6.1
pip3 install sphinx==1.3 --user
module add opencv-3.3.1-py34
pip3 install dlib --user

cd $SCRATCHDIR || exit 1
git clone https://gitlab.com/kohoutovice/bakalarka.git
cp /storage/brno2/home/xjurca08/frame_images_DB.tar.gz ./

tar -xvzf frame_images_DB.tar.gz

python3 bakalarka/facenet/Facial-Recognition-using-Facenet/rec_feat.py --file facenet.out0.pic -v $SCRATCHDIR/ --start 0 --stop 19409 &
p1=$!
python3 bakalarka/facenet/Facial-Recognition-using-Facenet/rec_feat.py --file facenet.out1.pic -v $SCRATCHDIR/ --start 19410 --stop 38819 &
p1=$!
python3 bakalarka/facenet/Facial-Recognition-using-Facenet/rec_feat.py --file facenet.out2.pic -v $SCRATCHDIR/ --start 38820 --stop 58229 &
p1=$!
python3 bakalarka/facenet/Facial-Recognition-using-Facenet/rec_feat.py --file facenet.out3.pic -v $SCRATCHDIR/ --start 58230 --stop 77639 &
p1=$!
python3 bakalarka/facenet/Facial-Recognition-using-Facenet/rec_feat.py --file facenet.out4.pic -v $SCRATCHDIR/ --start 77640 --stop 97049 &
p1=$!
python3 bakalarka/facenet/Facial-Recognition-using-Facenet/rec_feat.py --file facenet.out5.pic -v $SCRATCHDIR/ --start 97050 --stop 116460 &
p1=$!
python3 bakalarka/facenet/Facial-Recognition-using-Facenet/rec_feat.py --file facenet.out6.pic -v $SCRATCHDIR/ --start 116461 --stop 135870 &
p1=$!
python3 bakalarka/facenet/Facial-Recognition-using-Facenet/rec_feat.py --file facenet.out7.pic -v $SCRATCHDIR/ --start 135871 --stop 155280 &
p1=$!
python3 bakalarka/facenet/Facial-Recognition-using-Facenet/rec_feat.py --file facenet.out8.pic -v $SCRATCHDIR/ --start 155281 --stop 174690 &
p1=$!
python3 bakalarka/facenet/Facial-Recognition-using-Facenet/rec_feat.py --file facenet.out9.pic -v $SCRATCHDIR/ --start 174691 --stop 194100 &
p1=$!
python3 bakalarka/facenet/Facial-Recognition-using-Facenet/rec_feat.py --file facenet.out10.pic -v $SCRATCHDIR/ --start 194101 --stop 213511 &
p1=$!
python3 bakalarka/facenet/Facial-Recognition-using-Facenet/rec_feat.py --file facenet.out11.pic -v $SCRATCHDIR/ --start 213512 --stop 232921 &
p1=$!
python3 bakalarka/facenet/Facial-Recognition-using-Facenet/rec_feat.py --file facenet.out12.pic -v $SCRATCHDIR/ --start 232922 --stop 252331 &
p1=$!
python3 bakalarka/facenet/Facial-Recognition-using-Facenet/rec_feat.py --file facenet.out13.pic -v $SCRATCHDIR/ --start 252332 --stop 271741 &
p1=$!
python3 bakalarka/facenet/Facial-Recognition-using-Facenet/rec_feat.py --file facenet.out14.pic -v $SCRATCHDIR/ --start 271742 --stop 291151 &
p1=$!
python3 bakalarka/facenet/Facial-Recognition-using-Facenet/rec_feat.py --file facenet.out15.pic -v $SCRATCHDIR/ --start 291152 --stop 310562 &
p1=$!
python3 bakalarka/facenet/Facial-Recognition-using-Facenet/rec_feat.py --file facenet.out16.pic -v $SCRATCHDIR/ --start 310563 --stop 329972 &
p1=$!
python3 bakalarka/facenet/Facial-Recognition-using-Facenet/rec_feat.py --file facenet.out17.pic -v $SCRATCHDIR/ --start 329973 --stop 349382 &
p1=$!
python3 bakalarka/facenet/Facial-Recognition-using-Facenet/rec_feat.py --file facenet.out18.pic -v $SCRATCHDIR/ --start 349383 --stop 368792 &
p1=$!
python3 bakalarka/facenet/Facial-Recognition-using-Facenet/rec_feat.py --file facenet.out19.pic -v $SCRATCHDIR/ --start 368793 --stop 388202 &
p1=$!
python3 bakalarka/facenet/Facial-Recognition-using-Facenet/rec_feat.py --file facenet.out20.pic -v $SCRATCHDIR/ --start 388203 --stop 407612 &
p1=$!
python3 bakalarka/facenet/Facial-Recognition-using-Facenet/rec_feat.py --file facenet.out21.pic -v $SCRATCHDIR/ --start 407613 --stop 427023 &
p1=$!
python3 bakalarka/facenet/Facial-Recognition-using-Facenet/rec_feat.py --file facenet.out22.pic -v $SCRATCHDIR/ --start 427024 --stop 446433 &
p1=$!
python3 bakalarka/facenet/Facial-Recognition-using-Facenet/rec_feat.py --file facenet.out23.pic -v $SCRATCHDIR/ --start 446434 --stop 465843 &
p1=$!
python3 bakalarka/facenet/Facial-Recognition-using-Facenet/rec_feat.py --file facenet.out24.pic -v $SCRATCHDIR/ --start 465844 --stop 485253 &
p1=$!
python3 bakalarka/facenet/Facial-Recognition-using-Facenet/rec_feat.py --file facenet.out25.pic -v $SCRATCHDIR/ --start 485254 --stop 504663 &
p1=$!
python3 bakalarka/facenet/Facial-Recognition-using-Facenet/rec_feat.py --file facenet.out26.pic -v $SCRATCHDIR/ --start 504664 --stop 524074 &
p1=$!
python3 bakalarka/facenet/Facial-Recognition-using-Facenet/rec_feat.py --file facenet.out27.pic -v $SCRATCHDIR/ --start 524075 --stop 543484 &
p1=$!
python3 bakalarka/facenet/Facial-Recognition-using-Facenet/rec_feat.py --file facenet.out28.pic -v $SCRATCHDIR/ --start 543485 --stop 562894 &
p1=$!
python3 bakalarka/facenet/Facial-Recognition-using-Facenet/rec_feat.py --file facenet.out29.pic -v $SCRATCHDIR/ --start 562895 --stop 582304 &
p1=$!
python3 bakalarka/facenet/Facial-Recognition-using-Facenet/rec_feat.py --file facenet.out30.pic -v $SCRATCHDIR/ --start 582305 --stop 601714 &
p1=$!
python3 bakalarka/facenet/Facial-Recognition-using-Facenet/rec_feat.py --file facenet.out31.pic -v $SCRATCHDIR/ --start 601715 --stop 621125 &
p1=$!
wait $p0
cp facenet.out0.pic /storage/brno2/home/xjurca08/descs/facenet || export CLEAN_SCRATCH=false
wait $p1
cp facenet.out1.pic /storage/brno2/home/xjurca08/descs/facenet || export CLEAN_SCRATCH=false
wait $p2
cp facenet.out2.pic /storage/brno2/home/xjurca08/descs/facenet || export CLEAN_SCRATCH=false
wait $p3
cp facenet.out3.pic /storage/brno2/home/xjurca08/descs/facenet || export CLEAN_SCRATCH=false
wait $p4
cp facenet.out4.pic /storage/brno2/home/xjurca08/descs/facenet || export CLEAN_SCRATCH=false
wait $p5
cp facenet.out5.pic /storage/brno2/home/xjurca08/descs/facenet || export CLEAN_SCRATCH=false
wait $p6
cp facenet.out6.pic /storage/brno2/home/xjurca08/descs/facenet || export CLEAN_SCRATCH=false
wait $p7
cp facenet.out7.pic /storage/brno2/home/xjurca08/descs/facenet || export CLEAN_SCRATCH=false
wait $p8
cp facenet.out8.pic /storage/brno2/home/xjurca08/descs/facenet || export CLEAN_SCRATCH=false
wait $p9
cp facenet.out9.pic /storage/brno2/home/xjurca08/descs/facenet || export CLEAN_SCRATCH=false
wait $p10
cp facenet.out10.pic /storage/brno2/home/xjurca08/descs/facenet || export CLEAN_SCRATCH=false
wait $p11
cp facenet.out11.pic /storage/brno2/home/xjurca08/descs/facenet || export CLEAN_SCRATCH=false
wait $p12
cp facenet.out12.pic /storage/brno2/home/xjurca08/descs/facenet || export CLEAN_SCRATCH=false
wait $p13
cp facenet.out13.pic /storage/brno2/home/xjurca08/descs/facenet || export CLEAN_SCRATCH=false
wait $p14
cp facenet.out14.pic /storage/brno2/home/xjurca08/descs/facenet || export CLEAN_SCRATCH=false
wait $p15
cp facenet.out15.pic /storage/brno2/home/xjurca08/descs/facenet || export CLEAN_SCRATCH=false
wait $p16
cp facenet.out16.pic /storage/brno2/home/xjurca08/descs/facenet || export CLEAN_SCRATCH=false
wait $p17
cp facenet.out17.pic /storage/brno2/home/xjurca08/descs/facenet || export CLEAN_SCRATCH=false
wait $p18
cp facenet.out18.pic /storage/brno2/home/xjurca08/descs/facenet || export CLEAN_SCRATCH=false
wait $p19
cp facenet.out19.pic /storage/brno2/home/xjurca08/descs/facenet || export CLEAN_SCRATCH=false
wait $p20
cp facenet.out20.pic /storage/brno2/home/xjurca08/descs/facenet || export CLEAN_SCRATCH=false
wait $p21
cp facenet.out21.pic /storage/brno2/home/xjurca08/descs/facenet || export CLEAN_SCRATCH=false
wait $p22
cp facenet.out22.pic /storage/brno2/home/xjurca08/descs/facenet || export CLEAN_SCRATCH=false
wait $p23
cp facenet.out23.pic /storage/brno2/home/xjurca08/descs/facenet || export CLEAN_SCRATCH=false
wait $p24
cp facenet.out24.pic /storage/brno2/home/xjurca08/descs/facenet || export CLEAN_SCRATCH=false
wait $p25
cp facenet.out25.pic /storage/brno2/home/xjurca08/descs/facenet || export CLEAN_SCRATCH=false
wait $p26
cp facenet.out26.pic /storage/brno2/home/xjurca08/descs/facenet || export CLEAN_SCRATCH=false
wait $p27
cp facenet.out27.pic /storage/brno2/home/xjurca08/descs/facenet || export CLEAN_SCRATCH=false
wait $p28
cp facenet.out28.pic /storage/brno2/home/xjurca08/descs/facenet || export CLEAN_SCRATCH=false
wait $p29
cp facenet.out29.pic /storage/brno2/home/xjurca08/descs/facenet || export CLEAN_SCRATCH=false
wait $p30
cp facenet.out30.pic /storage/brno2/home/xjurca08/descs/facenet || export CLEAN_SCRATCH=false
wait $p31
cp facenet.out31.pic /storage/brno2/home/xjurca08/descs/facenet || export CLEAN_SCRATCH=false
