cd $SCRATCHDIR || exit 1
git clone https://gitlab.com/kohoutovice/bakalarka.git
cd bakalarka/gait/clasifier/
wget tensorcloud.cz/public/gait.pic
mkdir temp
TMPDIR=$(pwd)/temp
export TMPDIR
module add python36-modules-gcc
pip3 install  torch torchvision  --cache-dir $(pwd)/cache --root $(pwd)
PYTHONPATH=$(pwd)/software/python-3.6.2/gcc/lib/python3.6/site-packages/:$PYTHONPATH
python3.6 gait_rnn.py --load gait.pic
