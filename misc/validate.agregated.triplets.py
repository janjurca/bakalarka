import numpy as np
import argparse
import pickle
import os
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, auc
from scipy.optimize import brentq
from scipy.interpolate import interp1d
from sklearn.metrics import roc_curve

parser = argparse.ArgumentParser(description='')
parser.add_argument('--triplets', action='append', help='save')
parser.add_argument('--label', action='append', help='save')
parser.add_argument('--title', action='store', default="ROC", help='save')
parser.add_argument('--save', action='store', default=None, help='save')
parser.add_argument('--fpr', action='append', default=None, help='fpr')

args = parser.parse_args()

fprs = []
tprs = []
aucs = []
eers = []

for tfile in args.triplets:
    triplets = pickle.load(open(tfile,'rb'))

    scores = []
    labels = []

    for triplet in triplets:
        anchor = np.array(triplet[0].tolist())
        positive = np.array(triplet[1].tolist())
        negative = np.array(triplet[2].tolist())
        scores.append(np.linalg.norm(anchor-positive))
        labels.append(0)
        scores.append(np.linalg.norm(anchor-negative))
        labels.append(1)


    fpr, tpr, thresholds = roc_curve(np.array(labels),np.array(scores))
    roc_auc = auc(fpr, tpr)
    print(roc_auc)
    eer = brentq(lambda x : 1. - x - interp1d(fpr, tpr)(x), 0., 1.)
    print("EER",eer)
    eery = interp1d(fpr, tpr)(eer)
    print("EERY",eery)
    thresh = interp1d(fpr, thresholds)(eer)
    print("TRESH",thresh)

    all = 0
    good = 0
    for triplet in triplets:
        anchor = np.array(triplet[0].tolist())
        positive = np.array(triplet[1].tolist())
        negative = np.array(triplet[2].tolist())
        da = np.linalg.norm(anchor-positive)
        db = np.linalg.norm(anchor-negative)
        if da < thresh:
            good = good + 1
        all = all + 1

        if db >= thresh:
            good = good + 1
        all = all + 1

    print("ALL",all,"GOOD",good,"SCORE",good/all)
    fprs.append(fpr)
    tprs.append(tpr)
    aucs.append(roc_auc)
    eers.append((eer,eery))

colors = ['green','blue','red','cyan','magenta','yellow','black','white']

fig = plt.figure()
lw = 1
for i,(fpr,tpr,auc,eer) in enumerate(zip(fprs,tprs,aucs,eers)):
    plt.plot(fpr, tpr, color=colors[i%len(colors)], lw=lw, label=args.label[i] + ' (AUC = %0.2f)' % auc)
    plt.plot([eer[0]],eer[1],'bo',color="green")
    print(eer)
    if args.fpr:
        r = {
        "fpr":fpr,
        "tpr":tpr,
        "auc":auc,
        "eer":eer
        }
        print("Ukladam")
        pickle.dump(r,open(args.fpr[i],'wb'))

plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.0])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title(args.title)
plt.legend(loc="lower right")
plt.show()

if args.save:
    fig.savefig(args.save, bbox_inches='tight')
