import pickle
import glob
import numpy
import argparse
import os
import random

'''
persons = {
    "person_id"{
    "cam":[[frame1],frame2]]
    }
}
'''
persons = {}


parser = argparse.ArgumentParser(description='')
parser.add_argument('--glob', action='store', default="/home/rmin/Dokumenty/BUT_pedestrians/*/*/*/", help='folder')
parser.add_argument('--save', action='store', help='save')
args = parser.parse_args()
f = glob.glob(args.glob)
for i, folder in enumerate(f):
    print(i, "/", len(f))
    pose_files = list(glob.glob(folder + "/*pose"))
    pose_files.sort()

    path = (folder.rstrip("/") + "/").split("/")
    track_id = path[-2]
    person_id = path[-4]
    cam = path[-3]
    if person_id not in persons:
        persons[person_id] = {}
    if cam not in persons[person_id]:
        persons[person_id][cam] = []
    for pose_file in pose_files:
        persons[person_id][cam].append(pickle.load(open(pose_file,'rb')))

pickle.dump(persons,open(args.save,'wb'))
