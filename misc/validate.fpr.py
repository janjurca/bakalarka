import numpy as np
import argparse
import pickle
import os
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, auc

parser = argparse.ArgumentParser(description='')
parser.add_argument('--fpr', action='append', help='save')
parser.add_argument('--label', action='append', help='save')
parser.add_argument('--color', action='append', help='''['blue','green','red','cyan','magenta','yellow','black','white']''')
parser.add_argument('--style', action='append', help='''['-', '--', '-.', ':']''')
parser.add_argument('--lw', action='append', help='save')

parser.add_argument('--title', action='store', default="ROC", help='save')
parser.add_argument('--save', action='store', default=None, help='save')

args = parser.parse_args()

fprs = []
tprs = []
aucs = []

for ffile in args.fpr:
    f = pickle.load(open(ffile,'rb'))

    fprs.append(f["fpr"])
    tprs.append(f["tpr"])
    aucs.append(f["auc"])

colors = ['blue','green','red','cyan','magenta','yellow','black','white']
styles = ['-', '--', '-.', ':']
print(args.style)
fig = plt.figure()
lw = 2
for i,(fpr,tpr,auc) in enumerate(zip(fprs,tprs,aucs)):
    plt.plot(fpr, tpr, color=args.color[i], linestyle=styles[int(args.style[i])], lw=int(args.lw[i]), label=args.label[i] + ' (AUC = %0.2f)' % auc)

plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.0])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title(args.title)
plt.legend(loc="lower right")
plt.show()

if args.save:
    fig.savefig(args.save, bbox_inches='tight')
