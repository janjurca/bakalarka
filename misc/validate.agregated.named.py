import numpy as np
import argparse
import pickle
import os
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, auc
import random

parser = argparse.ArgumentParser(description='')
parser.add_argument('--load', action='store', default="/home/rmin/Dokumenty/extracted/person_descs/dlib/dlib.persons", help='load person file')
parser.add_argument('--good', action='store_true', help='load person file')

args = parser.parse_args()



scores = []
labels = []


def random_person_track(blacklist):
    person_ids = list(persons.keys())
    person_ids.remove(blacklist)
    person_id = random.choice(person_ids)
    cams = persons[person_id]
    cam = random.choice(list(cams.keys()))
    return persons[person_id][cam]

def cartesian_pairs(l):
    ret = []
    for i in range(l):
        for j in range(i,l):
            if i == j:
                continue
            ret.append((i,j))
    return ret

def get_triplet():
    '''
    ret = [
    (anchor,positive,negative)
    ]
    '''
    ret = []
    for person_id in persons.keys():
        tracks = persons[person_id]
        pairs = cartesian_pairs(len(tracks))
        for pair in pairs:
            anchor = tracks[pair[0]]
            positive = tracks[pair[1]]
            negative = random_person_track(person_id)
            ret.append((anchor,positive,negative))
    return ret


persons = pickle.load(open(args.load,'rb'))

treshhold = 0.45
good = 0
bad = 0
all = 0
dists1 = []
dists2 = []

for person_id in persons:
    tracks = list(persons[person_id].values())
    if len(tracks) < 2:
        continue
    pairs = cartesian_pairs(len(tracks))
    for pair in pairs:
        anchor = tracks[pair[0]]
        positive = tracks[pair[1]]
        negative = random_person_track(person_id)
        dist1 = np.linalg.norm(np.array(anchor)-np.array(positive))
        dists1.append(dist1)
        if dist1 > treshhold:
            bad = bad + 1
            print(person_id)
        else:
            good = good + 1
        all = all + 1

        dist2 = np.linalg.norm(np.array(anchor)-np.array(negative))
        dists2.append(dist2)
        if dist2 < treshhold:
            print(person_id)
            bad = bad + 1
        else:
            good = good + 1
        all = all + 1

print("GOOD",good)
print("BAD", bad)
print("ALL",all)
print("d1",np.array(dists1).mean())
print("d2",np.array(dists2).mean())
