import numpy as np
import argparse
import pickle
import os
import glob

'''
input file is pickled dict in structure

file = {
"filename with path": [descriptor]
}

output is pickle sorted by persons and tracks

out = {
"person_id": {
    "cam": [[frame1],[frame2]...], track1
    "cam": [[frame1],[frame2]...], track2
    }

}

'''
out = {}

parser = argparse.ArgumentParser(description='')
parser.add_argument('--file', action='store', default="/home/rmin/Dokumenty/extracted/descs/BUT_pedestrians/dlib/gait.pic", help='save')
parser.add_argument('--save', action='store' , required=True, help='save')
parser.add_argument('--dataset', action='store' , required=True, help='(but|mars)')

args = parser.parse_args()

descs = pickle.load(open(args.file,'rb'))
files = list(descs.keys())
files.sort()
for desc_file in files:
    print(desc_file)
    desc = descs[desc_file]
    if args.dataset == "but":
        path = desc_file.split("/")
        track_id = path[-2]
        person_id = path[-4]
        cam = path[-3]
    elif args.dataset == "mars":
        path = desc_file.split("/")
        "./dataset/bbox_test/0002/0002C1T0001F001.jpg"
        person_id = path[-2]
        cam = path[-1].split("C")[-1].split("T")[0]

    if person_id not in out:
        out[person_id] = {}
    if cam not in out[person_id]:
        out[person_id][cam] = []
    
    out[person_id][cam].append(desc)
    del descs[desc_file]

pickle.dump(out,open(args.save,'wb'))
