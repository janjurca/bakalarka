import pickle
import glob
import random
import numpy
import argparse
import os

parser = argparse.ArgumentParser(description='')
parser.add_argument('--load', action='store', default="/home/rmin/Dokumenty/extracted/gait.pic", help='folder')
parser.add_argument('--save', action='store', default="./gait.triplets.pic", help='save')
args = parser.parse_args()

def random_person_track(blacklist):
    person_ids = list(persons.keys())
    person_ids.remove(blacklist)
    person_id = random.choice(person_ids)
    cams = persons[person_id]
    cam = random.choice(list(cams.keys()))
    return persons[person_id][cam]

def cartesian_pairs(l):
    ret = []
    for i in range(l):
        for j in range(i,l):
            if i == j:
                continue
            ret.append((i,j))
    return ret

def get_triplet():
    '''
    ret = [
    (anchor,positive,negative)
    ]
    '''
    ret = []
    for person_id in persons.keys():
        tracks = persons[person_id]
        pairs = cartesian_pairs(len(tracks))
        for pair in pairs:
            anchor = tracks[pair[0]]
            positive = tracks[pair[1]]
            negative = random_person_track(person_id)
            ret.append((anchor,positive,negative))
    return ret


'''
triplets = [
(anchor,positive,negative),
(anchor,positive,negative),
(anchor,positive,negative),
(anchor,positive,negative),
]
'''
triplets = []

'''
persons = {
    "person_id": {
        "cam_id": [track]
    }
}
'''

persons = pickle.load(open(args.load,'rb'))

for person_id in persons:
    tracks = list(persons[person_id].values())
    if len(tracks) < 2:
        continue
    pairs = cartesian_pairs(len(tracks))
    for pair in pairs:
        anchor = tracks[pair[0]]
        positive = tracks[pair[1]]
        negative = random_person_track(person_id)
        triplets.append((anchor,positive,negative))



pickle.dump(triplets,open(args.save,'wb'))
