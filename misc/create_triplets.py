import pickle
import glob
import random
import numpy
import argparse
import os

parser = argparse.ArgumentParser(description='')
parser.add_argument('--load', action='store', default="/home/rmin/Dokumenty/extracted/gait.pic", help='folder')
parser.add_argument('--save', action='store', default="./gait.triplets.pic", help='save')
args = parser.parse_args()

def random_person_track(blacklist):
    person_ids = list(persons.keys())
    person_ids.remove(blacklist)
    id = random.choice(person_ids)
    tracks = persons[id]
    return random.choice(tracks)

def cartesian_pairs(l):
    ret = []
    for i in range(l):
        for j in range(i,l):
            if i == j:
                continue
            ret.append((i,j))
    return ret

def get_triplet():
    '''
    ret = [
    (anchor,positive,negative)
    ]
    '''
    ret = []
    for person_id in persons.keys():
        tracks = persons[person_id]
        pairs = cartesian_pairs(len(tracks))
        for pair in pairs:
            anchor = tracks[pair[0]]
            positive = tracks[pair[1]]
            negative = random_person_track(person_id)
            ret.append((anchor,positive,negative))
    return ret
persons = {}

for track_data in pickle.load(open(args.load,'rb')):
    track = track_data[0]
    person = track_data[1]
    if person not in persons:
        persons[person] = []
    persons[person].append(track)

t = get_triplet()

pickle.dump(t,open(args.save,'wb'))
