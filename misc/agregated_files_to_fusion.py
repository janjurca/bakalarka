import pickle
import glob
import random
import numpy as np
import argparse
import os

parser = argparse.ArgumentParser(description='')
parser.add_argument('--modality', action='append', help='files with agregated modalities')
parser.add_argument('--modality_size', action='append', help='sorted modality size')

parser.add_argument('--save', action='store', default="./fusion.agregated", help='save')
args = parser.parse_args()

'''
persons = {
    "person_id": {
        "cam_id": (mod1,mod2,mod3)
    }
}
'''

persons = {}

modalities = []
for file in args.modality:
    modalities.append(pickle.load(open(file,'rb')))

person_ids = set()
for modality in modalities:
    for person_id in modality.keys():
        person_ids.add(person_id)

#######add persons and tracks into persons
persons_tmp = {}
for person_id in person_ids:
    available_views = set()
    for i, modality in enumerate(modalities):
        try:
            tracks = modality[person_id].keys()
            for track in list(tracks):
                available_views.add(track)
        except Exception as e:
            print("unavailable person id",e, "in", i,"modality")
            pass
    persons_tmp[person_id] = available_views

#######finaly merge modalities
for person_id in persons_tmp:
    persons[person_id] = {}
    for view in persons_tmp[person_id]:

        vector = []
        for i, modality in enumerate(modalities):
            try:
                vector.append(modality[person_id][view])
            except KeyError as e:
                vector.append(np.zeros(int(args.modality_size[i])))
        vector = tuple(vector)
        persons[person_id][view] = vector

pickle.dump(persons,open(args.save,'wb'))
