EXTRACTED=/home/rmin/Dokumenty/extracted

python3.6 test_triplets.py --fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.mean.50 --eval $EXTRACTED/gait_train/gait.but.test.triplets.last.50 --ui --model cnn --weights $EXTRACTED/nets/nets/gait.triplet.torch.145.cnn.mean.50 -b 15000
python3.6 test_triplets.py --fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.mean.75 --eval $EXTRACTED/gait_train/gait.but.test.triplets.last.75 --ui --model cnn --weights $EXTRACTED/nets/nets/gait.triplet.torch.119.cnn.mean.75 -b 15000
python3.6 test_triplets.py --fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.mean.100 --eval $EXTRACTED/gait_train/gait.but.test.triplets.last.100 --ui --model cnn --weights $EXTRACTED/nets/nets/gait.triplet.torch.153.cnn.mean.100 -b 15000
python3.6 test_triplets.py --fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.mean.125 --eval $EXTRACTED/gait_train/gait.but.test.triplets.last.125 --ui --model cnn --weights $EXTRACTED/nets/nets/gait.triplet.torch.142.cnn.mean.125 -b 15000
python3.6 test_triplets.py --fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.mean.150 --eval $EXTRACTED/gait_train/gait.but.test.triplets.last.150 --ui --model cnn --weights $EXTRACTED/nets/nets/gait.triplet.torch.175.cnn.mean.150 -b 15000


python3.6 test_triplets.py --fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.nan.50 --eval $EXTRACTED/gait_train/gait.but.test.triplets.last.50 --ui --model cnnatm --weights $EXTRACTED/nets/nets/gait.triplet.torch.58.cnn.nan.50 -b 15000
python3.6 test_triplets.py --fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.nan.75 --eval $EXTRACTED/gait_train/gait.but.test.triplets.last.75 --ui --model cnnatm --weights $EXTRACTED/nets/nets/gait.triplet.torch.114.cnn.nan.75 -b 15000
python3.6 test_triplets.py --fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.nan.100 --eval $EXTRACTED/gait_train/gait.but.test.triplets.last.100 --ui --model cnnatm --weights $EXTRACTED/nets/nets/gait.triplet.torch.91.cnn.nan.100 -b 15000
python3.6 test_triplets.py --fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.nan.125 --eval $EXTRACTED/gait_train/gait.but.test.triplets.last.125 --ui --model cnnatm --weights $EXTRACTED/nets/nets/gait.triplet.torch.303.cnn.nan.125 -b 15000
python3.6 test_triplets.py --fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.nan.150 --eval $EXTRACTED/gait_train/gait.but.test.triplets.last.150 --ui --model cnnatm --weights $EXTRACTED/nets/nets/gait.triplet.torch.268.cnn.nan.150 -b 15000

######TODO
python3.6 test_triplets.py --fpr $EXTRACTED/nets/gait.triplet.fpr.rnn.last.50 --eval $EXTRACTED/gait_train/gait.but.test.triplets.last.50 --ui --model rnn --weights $EXTRACTED/nets/nets/gait.triplet.torch.3.rnn.last.50 -b 500
python3.6 test_triplets.py --fpr $EXTRACTED/nets/gait.triplet.fpr.rnn.last.75 --eval $EXTRACTED/gait_train/gait.but.test.triplets.last.75 --ui --model rnn --weights $EXTRACTED/nets/nets/gait.triplet.torch.175.rnn.last.75 -b 500
python3.6 test_triplets.py --fpr $EXTRACTED/nets/gait.triplet.fpr.rnn.last.100 --eval $EXTRACTED/gait_train/gait.but.test.triplets.last.100 --ui --model rnn --weights $EXTRACTED/nets/nets/gait.triplet.torch.930.rnn.last.100 -b 500
python3.6 test_triplets.py --fpr $EXTRACTED/nets/gait.triplet.fpr.rnn.last.125 --eval $EXTRACTED/gait_train/gait.but.test.triplets.last.125 --ui --model rnn --weights $EXTRACTED/nets/nets/gait.triplet.torch.42.rnn.last.125 -b 500
python3.6 test_triplets.py --fpr $EXTRACTED/nets/gait.triplet.fpr.rnn.last.150 --eval $EXTRACTED/gait_train/gait.but.test.triplets.last.150 --ui --model rnn --weights $EXTRACTED/nets/nets/gait.triplet.torch.30.rnn.last.150 -b 500


python3.6 test_triplets.py --fpr $EXTRACTED/nets/gait.triplet.fpr.rnn.nan.50 --eval $EXTRACTED/gait_train/gait.but.test.triplets.last.50 --ui --model rnnatm --weights $EXTRACTED/nets/nets/gait.triplet.torch.110.rnn.nan.50 -b 500
python3.6 test_triplets.py --fpr $EXTRACTED/nets/gait.triplet.fpr.rnn.nan.75 --eval $EXTRACTED/gait_train/gait.but.test.triplets.last.75 --ui --model rnnatm --weights $EXTRACTED/nets/nets/gait.triplet.torch.106.rnn.nan.75 -b 500
python3.6 test_triplets.py --fpr $EXTRACTED/nets/gait.triplet.fpr.rnn.nan.100 --eval $EXTRACTED/gait_train/gait.but.test.triplets.last.100 --ui --model rnnatm --weights $EXTRACTED/nets/nets/gait.triplet.torch.285.rnn.nan.100 -b 500
python3.6 test_triplets.py --fpr $EXTRACTED/nets/gait.triplet.fpr.rnn.nan.125 --eval $EXTRACTED/gait_train/gait.but.test.triplets.last.125 --ui --model rnnatm --weights $EXTRACTED/nets/nets/gait.triplet.torch.3415.rnn.nan.125 -b 500
python3.6 test_triplets.py --fpr $EXTRACTED/nets/gait.triplet.fpr.rnn.nan.150 --eval $EXTRACTED/gait_train/gait.but.test.triplets.last.150 --ui --model rnnatm --weights $EXTRACTED/nets/nets/gait.triplet.torch.1571.rnn.nan.150 -b 500


python3.6 test_triplets.py --fpr $EXTRACTED/nets/gait.triplet.fpr.rnn.mean.50 --eval $EXTRACTED/gait_train/gait.but.test.triplets.last.50 --ui --model rnn --weights $EXTRACTED/nets/nets/gait.triplet.torch.27.rnn.mean.50 -b 500
python3.6 test_triplets.py --fpr $EXTRACTED/nets/gait.triplet.fpr.rnn.mean.75 --eval $EXTRACTED/gait_train/gait.but.test.triplets.last.75 --ui --model rnn --weights $EXTRACTED/nets/nets/gait.triplet.torch.89.rnn.mean.75 -b 500
python3.6 test_triplets.py --fpr $EXTRACTED/nets/gait.triplet.fpr.rnn.mean.100 --eval $EXTRACTED/gait_train/gait.but.test.triplets.last.100 --ui --model rnn --weights $EXTRACTED/nets/nets/gait.triplet.torch.329.rnn.mean.100 -b 500
python3.6 test_triplets.py --fpr $EXTRACTED/nets/gait.triplet.fpr.rnn.mean.125 --eval $EXTRACTED/gait_train/gait.but.test.triplets.last.125 --ui --model rnn --weights $EXTRACTED/nets/nets/gait.triplet.torch.85.rnn.mean.125 -b 500
python3.6 test_triplets.py --fpr $EXTRACTED/nets/gait.triplet.fpr.rnn.mean.150 --eval $EXTRACTED/gait_train/gait.but.test.triplets.last.150 --ui --model rnn --weights $EXTRACTED/nets/nets/gait.triplet.torch.149.rnn.mean.150 -b 500

python3.6 test_triplets.py --fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.rnn.50 --eval $EXTRACTED/gait_train/gait.but.test.triplets.last.50 --ui --model cnnrnn --weights $EXTRACTED/nets/nets/gait.triplet.torch.7.cnn.rnn.50 -b 500
python3.6 test_triplets.py --fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.rnn.75 --eval $EXTRACTED/gait_train/gait.but.test.triplets.last.75 --ui --model cnnrnn --weights $EXTRACTED/nets/nets/gait.triplet.torch.23.cnn.rnn.75 -b 500
python3.6 test_triplets.py --fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.rnn.100 --eval $EXTRACTED/gait_train/gait.but.test.triplets.last.100 --ui --model cnnrnn --weights $EXTRACTED/nets/nets/gait.triplet.torch.9.cnn.rnn.100 -b 500
python3.6 test_triplets.py --fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.rnn.125 --eval $EXTRACTED/gait_train/gait.but.test.triplets.last.125 --ui --model cnnrnn --weights $EXTRACTED/nets/nets/gait.triplet.torch.22.cnn.rnn.125 -b 500
python3.6 test_triplets.py --fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.rnn.150 --eval $EXTRACTED/gait_train/gait.but.test.triplets.last.150 --ui --model cnnrnn --weights $EXTRACTED/nets/nets/gait.triplet.torch.85.cnn.rnn.150 -b 500



python3.6 ../../misc/validate.fpr.py \
--fpr $EXTRACTED/nets/gait.triplet.fpr.rnn.mean.50 --label "50 snímků"  --color green --style=0 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.rnn.mean.75 --label "75 snímků" --color green --style=1 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.rnn.mean.100 --label "100 snímků" --color green --style=2 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.rnn.mean.125 --label "125 snímků" --color green --style=3 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.rnn.mean.150 --label "150 snímků" --color green --style=1 --lw 2 \
--save roc.rnn.mean.pdf \
--title "ROC RNN + MEAN" \

python3.6 ../../misc/validate.fpr.py \
--fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.mean.50 --label "50 snímků"  --color green --style=0 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.mean.75 --label "75 snímků" --color green --style=1 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.mean.100 --label "100 snímků" --color green --style=2 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.mean.125 --label "125 snímků" --color green --style=3 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.mean.150 --label "150 snímků" --color green --style=1 --lw 2 \
--save roc.cnn.mean.pdf \
--title "ROC CNN + MEAN" \
python3.6 ../../misc/validate.fpr.py \
--fpr $EXTRACTED/nets/gait.triplet.fpr.rnn.nan.50 --label "50 snímků"  --color green --style=0 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.rnn.nan.75 --label "75 snímků" --color green --style=1 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.rnn.nan.100 --label "100 snímků" --color green --style=2 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.rnn.nan.125 --label "125 snímků" --color green --style=3 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.rnn.nan.150 --label "150 snímků" --color green --style=1 --lw 2 \
--save roc.rnn.nan.pdf \
--title "ROC RNN + NAN" \
python3.6 ../../misc/validate.fpr.py \
--fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.nan.50 --label "50 snímků"  --color green --style=0 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.nan.75 --label "75 snímků" --color green --style=1 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.nan.100 --label "100 snímků" --color green --style=2 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.nan.125 --label "125 snímků" --color green --style=3 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.nan.150 --label "150 snímků" --color green --style=1 --lw 2 \
--save roc.cnn.nan.pdf \
--title "ROC CNN + NAN" \
python3.6 ../../misc/validate.fpr.py \
--fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.rnn.50 --label "50 snímků"  --color purple --style=0 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.rnn.75 --label "75 snímků" --color purple --style=1 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.rnn.100 --label "100 snímků" --color purple --style=2 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.rnn.125 --label "125 snímků" --color purple --style=3 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.rnn.150 --label "150 snímků" --color purple --style=1 --lw 2 \
--save roc.cnn.rnn.pdf \
--title "ROC CNN + RNN + MEAN" \

python3.6 ../../misc/validate.fpr.py \
--fpr $EXTRACTED/nets/gait.triplet.fpr.rnn.mean.50 --label "rnn.mean 50 snímků"  --color green --style=0 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.rnn.mean.75 --label "rnn.mean 75 snímků" --color green --style=1 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.rnn.mean.100 --label "rnn.mean 100 snímků" --color green --style=2 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.rnn.mean.125 --label "rnn.mean 125 snímků" --color green --style=3 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.rnn.mean.150 --label "rnn.mean 150 snímků" --color green --style=1 --lw 2 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.mean.50 --label "cnn.mean 50 snímků"  --color red --style=0 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.mean.75 --label "cnn.mean 75 snímků" --color red --style=1 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.mean.100 --label "cnn.mean 100 snímků" --color red --style=2 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.mean.125 --label "cnn.mean 125 snímků" --color red --style=3 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.mean.150 --label "cnn.mean 150 snímků" --color red --style=1 --lw 2 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.rnn.nan.50 --label "rnn.nan 50 snímků"  --color orange --style=0 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.rnn.nan.75 --label "rnn.nan 75 snímků" --color orange --style=1 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.rnn.nan.100 --label "rnn.nan 100 snímků" --color orange --style=2 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.rnn.nan.125 --label "rnn.nan 125 snímků" --color orange --style=3 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.rnn.nan.150 --label "rnn.nan 150 snímků" --color orange --style=1 --lw 2 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.nan.50 --label "cnn.nan 50 snímků"  --color yellow --style=0 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.nan.75 --label "cnn.nan 75 snímků" --color yellow --style=1 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.nan.100 --label "cnn.nan 100 snímků" --color yellow --style=2 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.nan.125 --label "cnn.nan 125 snímků" --color yellow --style=3 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.nan.150 --label "cnn.nan 150 snímků" --color yellow --style=1 --lw 2 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.rnn.50 --label "50 snímků"  --color purple --style=0 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.rnn.75 --label "75 snímků" --color purple --style=1 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.rnn.100 --label "100 snímků" --color purple --style=2 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.rnn.125 --label "125 snímků" --color purple --style=3 --lw 1 \
--fpr $EXTRACTED/nets/gait.triplet.fpr.cnn.rnn.150 --label "150 snímků" --color purple --style=1 --lw 2 \
