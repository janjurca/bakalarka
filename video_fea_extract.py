#!/usr/bin/python
import sys
import os
import dlib
import glob
import cv2
from utils.DLIBVideoLoader import DLIBVideoLoader
from utils.DLIBVideoLoader import Compatibility
from utils.TupleLoader import TupleLoader
from utils.FaceDescAgregator import FaceDescAgregator
import pickle
from time import sleep
import argparse

def main():
    # Load all the models we need: a detector to find the faces, a shape predictor
    # to find face landmarks so we can precisely localize the face, and finally the
    # face recognition model.
    detector = dlib.get_frontal_face_detector()
    sp = dlib.shape_predictor(args.predictor)
    facerec = dlib.face_recognition_model_v1(args.model)
    faces = {}
    files = glob.glob("frame_images_DB/*/*/*.jpg")
    count = len(files)
    for i,file in enumerate(files):
        if args.start:
            if i < args.start:
                print("Skipped",i,"/",count,file)
                continue

        l = file.split("/")
        if not l[1] in faces:
            faces[l[1]] = dict()
        if not l[2] in faces[l[1]]:
            faces[l[1]][l[2]] = dict()

        print(i,"/",count,file)
        img = dlib.load_rgb_image(file)

        #win = dlib.image_window()
        dets = detector(img, 1)
        if len(dets) >= 1:
            face = dets[0]
            shape = sp(img, face)

            #win.clear_overlay()
            #win.add_overlay(face)
            #win.add_overlay(shape)

            face_descriptor = facerec.compute_face_descriptor(img, shape)
            desc = FaceDescAgregator.dlibVect_to_numpyNDArray(face_descriptor)
            faces[l[1]][l[2]][file] = desc

        if i%5000 == 0 :
            pickle.dump(faces,open(args.file,'wb'))
        if args.stop:
            if i >= args.stop:
                break

    pickle.dump(faces,open(args.file,'wb'))

example_usage = '''Call this program like this:\n
   ./face_recognition.py shape_predictor_5_face_landmarks.dat dlib_face_recognition_resnet_model_v1.dat ../examples/faces\n
You can download a trained facial shape predictor and recognition model from:\n
    http://dlib.net/files/shape_predictor_5_face_landmarks.dat.bz2\n
    http://dlib.net/files/dlib_face_recognition_resnet_model_v1.dat.bz2'''

parser = argparse.ArgumentParser(example_usage)
parser.add_argument('-p', '--predictor', action='store', help='predictor .dat file', default='')
parser.add_argument('-m', '--model', action='store', help='model .dat file', default='config.json')
parser.add_argument("-f", "--file",  action="store", type=str, help="pickle target")
parser.add_argument("-v", "--path", action="store",default="", type=str, help="path with multiple videos")
parser.add_argument("-s", "--start", action="store",default=None, type=int, help="first index")
parser.add_argument("-c", "--stop", action="store",default=None, type=int, help="last index")
args = parser.parse_args()
main()
