import matplotlib.pyplot as plt
import numpy as np
from matplotlib import colors
from matplotlib.ticker import PercentFormatter

# Fixing random state for reproducibility
np.random.seed(19680801)

N_points = 50

# Generate a normal distribution, center at x=0 and y=5
x = np.linspace(0, N_points, num=N_points+1)
y = np.random.randn(N_points+1)

plt.figure()

# We can set the number of bins with the `bins` kwarg

plt.bar(x, y, width=0.8, bottom=None)
plt.show()
