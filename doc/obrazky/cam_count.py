import glob
import numpy as np
import matplotlib.pyplot as plt
import argparse
import os, os.path


parser = argparse.ArgumentParser(description='')
parser.add_argument('--butfolder', action='store', default=None, help='folder')
parser.add_argument('--pridfolder', action='store', default=None, help='folder')
parser.add_argument('--marsfolder', action='store', default=None, help='folder')
parser.add_argument('--dukefolder', action='store', default=None, help='folder')
parser.add_argument('--ilidsfolder', action='store', default=None, help='folder')

parser.add_argument('--save', action='store', default=None, help='save')
args = parser.parse_args()


fig = plt.figure()
ax = fig.add_subplot(111)
width = 0.20
offset = -0.30
if args.butfolder:
    x = []
    for track in glob.glob(args.butfolder.rstrip("/") + "/*/*/"):
        x.append(len(os.listdir(track)))
        print(track, x[-1])
    h ,b = np.histogram(x, bins=3)
    print(h)
    print(b)
    ax.bar(np.array([2,3,4])+offset, h, width=width-0.05,label="BUT pedestrians ")
    offset = offset + width
if args.pridfolder:
    '''persons = {
    "id": {"cam"}
    }'''
    persons = {}
    with open(args.pridfolder,'rb') as f:
        for line in f.read().splitlines():
            person_id = str(line).split("/")[-2]
            cam_id = str(line).split("/")[-3]
            if person_id not in persons.keys():
                persons[person_id] = set()
            persons[person_id].add(cam_id)

    x = []
    for person_id in persons:
        x.append(len(persons[person_id]))

    h ,b = np.histogram(x, bins=2)
    print(h)
    print(b)
    ax.bar(np.array([1,2])+offset, h, width=width-0.05,label="PRID11")
    offset = offset + width
if args.marsfolder:
    '''persons = {
    "id": {"cam"}
    }
    5864  08-07-2016 18:15   bbox_train/0001/0001C1T0001F008.jpg
    '''
    persons = {}
    with open(args.marsfolder,'rb') as f:
        for line in f.read().splitlines():
            person_id = str(line).split("/")[-2]
            cam_id = str(line).split("/")[-1].split("T")[0].split("C")[-1]
            if person_id not in persons.keys():
                persons[person_id] = set()
            persons[person_id].add(cam_id)
        print("LEN",len(persons))
    x = []
    for person_id in persons:
        x.append(len(persons[person_id]))

    h ,b = np.histogram(x, bins=6)
    print(h)
    print(b)
    ax.bar(np.array([1,2,3,4,5,6])+offset, h, width=width-0.05,label="MARS")
    offset = offset + width


if args.ilidsfolder:

    '''persons = {
    "id": {"cam"}
    }
    i-LIDS-VID/images/cam1/person010/cam1_person010.png
    '''
    persons = {}
    with open(args.ilidsfolder,'rb') as f:
        for line in f.read().splitlines():
            person_id = str(line).split("/")[-2]
            cam_id = str(line).split("/")[-3]
            if person_id not in persons.keys():
                persons[person_id] = set()
            persons[person_id].add(cam_id)

    x = []
    for person_id in persons:
        x.append(len(persons[person_id]))

    h ,b = np.histogram(x, bins=2)
    print(h)
    print(b)
    ax.bar(np.array([1,2])+offset, h, width=width-0.05,label="ILIDS-VID")
    offset = offset + width


ax.legend()

plt.xticks(np.array([1,2,3,4,5,6]))
plt.xlabel('Počet kamer zabírajících jednu identitu')
plt.ylabel('Počet identit s daným počtem kamer')
plt.title("Rozložení počtu kamer zabírajících jednu identitu")
plt.show()

if args.save:
    fig.savefig(args.save, bbox_inches='tight')
