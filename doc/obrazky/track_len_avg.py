import glob
import numpy as np
import matplotlib.pyplot as plt
import argparse
import os, os.path


parser = argparse.ArgumentParser(description='')
parser.add_argument('--glob', action='store', default=None, help='folder')
args = parser.parse_args()

x = []
for track in glob.glob(args.glob):
    print(track)
    x.append(int((len([name for name in os.listdir(track) if os.path.isfile(os.path.join(track, name))])/2)))

print(np.array(x).mean())
