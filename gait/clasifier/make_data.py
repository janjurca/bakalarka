import pickle
import glob
import numpy
import argparse
import os
import random


def get_tracks(root):
    dirs = []
    for root, directories, filenames in os.walk(root):
        for directory in directories:
            if "entry" in root or "overview" in root or "e112" in root or "d105" in root:
                dirs.append(os.path.join(root, directory))
    return dirs


parser = argparse.ArgumentParser(description='')
parser.add_argument('--folder', action='store', default="/home/rmin/Dokumenty/BUT_pedestrians/", help='folder')
parser.add_argument('--persons', action='store', default=None, help='persons file')
parser.add_argument('--track_len', action='store',type=int, default=75,help='Max track len (frames) ')
parser.add_argument('--cut', action='store_true' ,help='cut to max frame length  and not proccess rest of the track ')
parser.add_argument('--static_pose', action='store_true' ,help='static pose data ')
parser.add_argument('--shuffle', action='store_true' ,help='shuffle ')
parser.add_argument('--skip', action='store_true' ,help='skip smaller tracks ')
parser.add_argument('--save', action='store', default=None, help='save')
parser.add_argument('--save_triplets', action='store', default=None, help='save')

args = parser.parse_args()

'''
data = [
([[pose_frame_1],[pose_frame_2]],label-person_id)
]

tracks = {
'trackid': [[frame1],[frame2]]
}

track_mapping = {
trackid:person_id
}

cam_mapping = {
trackid:cam_id
}

'''
data = []
track_mapping = {}
cam_mapping = {}
tracks = {}

person_ids = set()

if args.persons:
    person_count = 0
    track_id = 0
    persons = pickle.load(open(args.persons,'rb'))
    for person_id in persons:
        person_count = person_count + 1
        for cam in persons[person_id]:
            track_mapping[track_id] = person_id
            cam_mapping[track_id] = cam
            track = []
            for frame in persons[person_id][cam]:
                f = []
                for point in frame:
                    f.extend(point)
                track.append(f)
            tracks[track_id] = track
            track_id = track_id + 1
else:
    for i, track_folder in enumerate(get_tracks(args.folder)):
        track_id = track_folder.split("/")[-1]
        person_id = track_folder.split("/")[-3]
        cam = track_folder.split("/")[-2]

        track_mapping[track_id] = person_id
        cam_mapping[track_id] = cam
        files = glob.glob(track_folder + "/*.pose")
        files.sort()
        track = []
        for j, file in enumerate(files):
            frame = pickle.load(open(file,'rb'))
            f = []
            for point in frame:
                f.extend(point)
            if args.static_pose:
                if len(track) == 0:
                    track.append(f)
                else:
                    track.append(track[:][0])
            else:
                track.append(f)
        tracks[track_id] = track
        print(i)

if args.shuffle:
    for track_id in tracks:
        random.shuffle(tracks[track_id])


print("tracks count",len(tracks))
for track_id in track_mapping.keys():
    person_ids.add(track_mapping[track_id])

person_ids = list(person_ids)

for track_id in tracks.keys():
    l = len(tracks[track_id])
    if args.cut:
        chunks_count = 1
    else:
        chunks_count = int(l/args.track_len)

    for j in range(chunks_count):
        if args.save:
            try:
                data.append((tracks[track_id][args.track_len*j:j*args.track_len + args.track_len],track_mapping[track_id],cam_mapping[track_id]))
            except Exception as e:
                data.append((tracks[track_id][args.track_len*j:],track_mapping[track_id],cam_mapping[track_id]))
        else:
            try:
                data.append((tracks[track_id][args.track_len*j:j*args.track_len + args.track_len],person_ids.index(track_mapping[track_id])))
            except Exception as e:
                data.append((tracks[track_id][args.track_len*j:],person_ids.index(track_mapping[track_id])))
    if chunks_count != 0:
        if len(data[-1][0])< args.track_len and (not args.cut or args.skip) :
            del data[-1]
    else:
        print("not enought track lenght",l)

print("Count of generated data",len(data))
if args.save:
    pickle.dump(data,open(args.save,'wb'))
    exit(0)

def get_tracks_with_person_id(id):
    traky = []
    for trak in data:
        if trak[1] == id:
            traky.append(trak[0])
    return traky

def get_random_track_instead_of(id):
    selected_id = id
    item = None
    while selected_id == id:
        item = random.choice(data)
        selected_id = item[1]
    return item[0]

def cartesian_pairs(l):
    ret = []
    for i in range(l):
        for j in range(i,l):
            if i == j:
                continue
            ret.append((i,j))
    return ret

if args.save_triplets:
    triplets = []
    print("Len of track mapping",len(track_mapping))
    print("Len of person_ids",len(person_ids))

    for i in range(len(person_ids)):
        print(i)
        ctracks = get_tracks_with_person_id(i)
        cartesian_indexes = cartesian_pairs(len(ctracks))
        for index in cartesian_indexes:
            anchor = ctracks[index[0]]
            positive = ctracks[index[1]]
            negative = get_random_track_instead_of(i)
            triplets.append((anchor,positive,negative))

    #print("==============================")
    #print(anchor)
    #print("==============================")
    #print(positive)
    #print("==============================")
    #print(negative)
    pickle.dump(triplets, open(args.save_triplets,'wb'))
