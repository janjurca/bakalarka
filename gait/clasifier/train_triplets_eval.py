import matplotlib
from matplotlib import pyplot as plt
import argparse
import os
import shutil
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.autograd import Variable
import torch.backends.cudnn as cudnn
from network import *

import numpy as np
from tripletDataset import TripletNetworkDatasetUnified
from sklearn import metrics
from sklearn.metrics import roc_curve, auc

import argparse

def static_vars(**kwargs):
    def decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func
    return decorate


@static_vars(tc=1)
def plotroc(fpr,tpr):
    plt.clf()
    plt.plot(fpr, tpr, lw=2, label="net")
    plt.legend(loc="lower right")
    plt.pause(0.05)
    #plt.savefig("figure" + str(plotroc.tc) + ".jpg")
    plotroc.tc = plotroc.tc + 1




@static_vars(losses=[])
@static_vars(aucs=[])
def test(test_loader, tnet, criterion):
    aucs = []
    losses = []
    total_labels = []
    total_dists = []
    with torch.no_grad():
        # switch to evaluation mode
        tnet.eval()
        data = pickle.load(open(args.load,'rb'))
        agregated = {}
        for track,person_id,cam_id in data:
            #if person_id in agregated:
            #    if cam_id in agregated[person_id]:
            #        continue
            anchor = torch.tensor([track])
            positive = anchor
            negative = anchor
            lenghts = 1
            # compute output
            dista, distb, em1, em2, em3 = tnet(anchor,positive,negative,lenghts)
            if person_id not in agregated:
                agregated[person_id] = {}
            if cam_id not in agregated[person_id]:
                agregated[person_id][cam_id] = []

            print(person_id,cam_id)
            agregated[person_id][cam_id].append(em1.squeeze().tolist())
        ret = {}
        for person_id in agregated:
            ret[person_id] = {}
            for cam_id in agregated[person_id]:
                t = torch.tensor(agregated[person_id][cam_id])
                ret[person_id][cam_id] = t.mean(0)
        pickle.dump(ret,open(args.save,'wb'))
        exit(0)
    return 0


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("-l", "--load", action="store", type=str, help="source file")
    parser.add_argument("-e", "--eval", action="append", type=str, help="source file")
    parser.add_argument("--save", action="store", type=str, help="source file")

    parser.add_argument("-b", "--batchsize", action="store",default=853, type=int, help="source file")
    parser.add_argument("-n", "--num_classes", action="store",default=625, type=int, help="source file")
    parser.add_argument( "--ui", action="store_true", help="source file")
    parser.add_argument("--weights", action="store",default=None, type=str, help="torch wieghts file")
    parser.add_argument("--model", action="store",required=True, type=str, help="model to use")

    args = parser.parse_args()


    if args.ui:
        plt.figure()
        plt.show(False)
        plt.draw()
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title('Receiver operating characteristic example')
        plt.legend(loc="lower right")

    # Hyper-parameters
    input_size = 36
    hidden_size = 512
    num_layers = 2
    num_classes = int(args.num_classes)

    train_loader = None
    test_loader = None

    models = {
    "resnet": ResNet(BasicBlock, [2,2,2,2],1024).to(device),
    "cnn":CNN(input_size, hidden_size, num_layers).to(device),
    "rnn":RNN(input_size, hidden_size, num_layers).to(device),
    "rnnatm":RNNATM(input_size, hidden_size, num_layers).to(device),
    "cnnatm": CNNATM(input_size, hidden_size, num_layers).to(device),
    "cnnrnn": CNNRNN(input_size, hidden_size, num_layers).to(device),
    "cnnrnnatm":CNNRNNATM(input_size, hidden_size, num_layers).to(device),
    }

    model = models[args.model]

    tnet = Tripletnet(model)

    criterion = nn.TripletMarginLoss(margin=1.0, p=2)
    optimizer = optim.Adam(tnet.parameters(), lr=0.0001)

    if args.weights:
        load_checkpoint(tnet, optimizer, args.weights)

    n_parameters = sum([p.data.nelement() for p in tnet.parameters()])
    print('  + Number of params: {}'.format(n_parameters))

    for epoch in range(1, 50000 + 1):
        # train for one epoch
        #train(train_loader, tnet, criterion, optimizer, epoch)
        # evaluate on validation set
        test(test_loader, tnet, criterion)
        break
        #state = {'epoch': epoch + 1, 'state_dict': tnet.state_dict(), 'optimizer': optimizer.state_dict() }
        #torch.save(state, "gait.triplet.torch." + str(epoch))
