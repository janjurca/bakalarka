import torch
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms
import numpy as np
import argparse
import pickle
import os
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, auc
import glob
from network import *
from tripletDataset import *

parser = argparse.ArgumentParser(description='')
parser.add_argument('--file', action='store', default=None, help='save')
parser.add_argument('--weights', action='store', default=None, help='save')
parser.add_argument('--save', action='store', default="gait.agregated", help='save')
args = parser.parse_args()

# Device configuration
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

# Hyper-parameters
input_size = 36
hidden_size = 512
num_layers = 1
num_classes = 275
num_epochs = 5000
learning_rate = 0.001
step_size = 700
pad = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]



def load_checkpoint(model, optimizer, filename='checkpoint.pth.tar'):
    # Note: Input model & optimizer should be pre-defined.  This routine only updates their states.
    start_epoch = 0
    if os.path.isfile(filename):
        print("=> loading checkpoint '{}'".format(filename))
        checkpoint = torch.load(filename,map_location='cpu')
        model.load_state_dict(checkpoint['state_dict'])
        optimizer.load_state_dict(checkpoint['optimizer'])
        print("=> loaded checkpoint '{}' (epoch {})"
                  .format(filename, checkpoint['epoch']))
    else:
        print("=> no checkpoint found at '{}'".format(filename))

    return model, optimizer

def get_tracks(root):
    dirs = []
    for root, directories, filenames in os.walk(root):
        for directory in directories:
            if "entry" in root or "overview" in root or "e112" in root or "d105" in root:
                dirs.append(os.path.join(root, directory))
    return dirs

model = Evalnet().to(device)
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

if args.weights:
    load_checkpoint(model, optimizer,  args.weights)

test_loader = torch.utils.data.DataLoader(
    TripletNetworkDatasetUnified(data=args.file,drop=True),
    batch_size=1, shuffle=False)

ret = []
count = len(triplets)

for i, triplet in enumerate(triplets):
    anchor, positive, negative = triplet

    anchor = torch.tensor(anchor).float().to(device)
    positive = torch.tensor(positive).float().to(device)
    negative = torch.tensor(negative).float().to(device)

    anchor = model(anchor,1)
    positive = model(positive,1)
    negative = model(negative,1)

    ret.append((anchor,positive,negative))
    print(i,"/",count)

pickle.dump(ret, open(args.save,'wb'))
