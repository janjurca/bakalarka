
import pickle
import torch
from torch.utils.data import DataLoader,Dataset
import numpy as np
import torch.utils.data
import random

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


class TripletNetworkDatasetUnified(Dataset):
    def __init__(self,data=["/home/rmin/Dokumenty/extracted/agregated/fusion.agregated.triplets"],drop=False):
        self.data = []
        for file in data:
            self.data.extend(pickle.load(open(file,'rb')))
        self.drop = drop

    def __getitem__(self,index):
        anchor, positive, negative = self.data[index]

        anchor = torch.tensor(anchor).float().to(device)
        positive = torch.tensor(positive).float().to(device)
        negative = torch.tensor(negative).float().to(device)

        return anchor, positive, negative, torch.tensor(75)

    def __len__(self):
        return len(self.data)
