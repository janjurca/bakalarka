import matplotlib
from matplotlib import pyplot as plt
import argparse
import os
import shutil
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.autograd import Variable
import torch.backends.cudnn as cudnn
from network import *

import numpy as np
from tripletDataset import TripletNetworkDatasetUnified
from sklearn import metrics
from sklearn.metrics import roc_curve, auc

import argparse

def static_vars(**kwargs):
    def decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func
    return decorate

@static_vars(losses=[])
def train(train_loader, tnet, criterion, optimizer, epoch):
    losses = []
    # switch to train mode
    tnet.train()
    for batch_idx, (anchor,positive,negative,lenghts) in enumerate(train_loader):

        # compute output
        dista, distb, embedded_x, embedded_y, embedded_z = tnet(anchor,positive,negative,lenghts)

        loss_triplet = criterion(embedded_x, embedded_y, embedded_z)
        loss_embedd = embedded_x.norm(2) + embedded_y.norm(2) + embedded_z.norm(2)
        #loss = loss_triplet + 0.001 * loss_embedd
        loss = loss_triplet

        # compute gradient and do optimizer step
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        losses.append(float(loss))
        print('Train Epoch: {} [{}/{}]\t Loss: {:.4f} ({:.4f}) \t'.format(
            epoch, batch_idx * len(anchor), len(train_loader.dataset),
            loss, loss_triplet))
    train.losses.append(torch.tensor(losses).mean().item())
    print("EPOCH LOSS:",torch.tensor(losses).mean())
    print(train.losses)



@static_vars(tc=1)
def plotroc(fpr,tpr):
    plt.clf()
    plt.plot(fpr, tpr, lw=2, label="net")
    plt.legend(loc="lower right")
    plt.pause(0.05)
    #plt.savefig("figure" + str(plotroc.tc) + ".jpg")
    plotroc.tc = plotroc.tc + 1




@static_vars(losses=[])
@static_vars(aucs=[])
def test(test_loader, tnet, criterion):
    aucs = []
    losses = []
    total_labels = []
    total_dists = []
    with torch.no_grad():
        # switch to evaluation mode
        tnet.eval()
        for batch_idx, (anchor,positive,negative,lenghts) in enumerate(test_loader):
            # compute output
            dista, distb, em1, em2, em3 = tnet(anchor,positive,negative,lenghts)
            test_loss =  criterion(em1, em2, em3).item()

            pos_labels = torch.tensor(np.full((1, len(distb)), 1).squeeze())
            neg_labels = torch.tensor(np.full((1, len(dista)), 0).squeeze())

            labels = torch.cat((pos_labels,neg_labels),dim=0)
            dists = torch.cat((distb,dista),dim=0)
            total_labels.extend(labels.tolist())
            total_dists.extend(dists.tolist())
            fpr, tpr, _ = roc_curve(np.array(labels.tolist()),np.array(dists.tolist()))
            roc_auc = auc(fpr, tpr)
            if args.ui:
                plotroc(fpr,tpr)

            print('Test set:[{}/{}] loss: {:.4f}, AUC: {:.2f}%'.format(batch_idx * len(anchor), len(test_loader.dataset),test_loss, roc_auc))
            aucs.append(roc_auc)
            losses.append(test_loss)

        fpr, tpr, _ = roc_curve(np.array(total_labels),np.array(total_dists))
        roc_auc = auc(fpr, tpr)
        test.aucs.append(roc_auc)
        test.losses.append(torch.tensor(losses).mean().item())
        print("FULL AUC:",roc_auc,"EPOCH AUCS:",torch.tensor(aucs).mean(),"LOSS:",torch.tensor(losses).mean())
        print(test.losses)
        print(test.aucs)
        print("MAX score",np.array(test.aucs).max(),"Index:",np.array(test.aucs).argmax())

    return roc_auc


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("-l", "--load", action="append", type=str, help="source file")
    parser.add_argument("-e", "--eval", action="append", type=str, help="source file")
    parser.add_argument("-b", "--batchsize", action="store",default=853, type=int, help="source file")
    parser.add_argument("-n", "--num_classes", action="store",default=625, type=int, help="source file")
    parser.add_argument( "--ui", action="store_true", help="source file")
    parser.add_argument("--weights", action="store",default=None, type=str, help="torch wieghts file")
    parser.add_argument("--model", action="store",required=True, type=str, help="model to use")

    args = parser.parse_args()


    if args.ui:
        plt.figure()
        plt.show(False)
        plt.draw()
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title('Receiver operating characteristic example')
        plt.legend(loc="lower right")

    # Hyper-parameters
    input_size = 36
    hidden_size = 512
    num_layers = 2
    num_classes = int(args.num_classes)

    train_loader = torch.utils.data.DataLoader(
        TripletNetworkDatasetUnified(data=args.load,drop=True),
        batch_size=args.batchsize, shuffle=True)
    test_loader = torch.utils.data.DataLoader(
        TripletNetworkDatasetUnified(data=args.eval,drop=True),
        batch_size=args.batchsize, shuffle=False)

    models = {
    "resnet": ResNet(BasicBlock, [2,2,2,2],1024).to(device),
    "cnn":CNN(input_size, hidden_size, num_layers).to(device),
    "rnn":RNN(input_size, hidden_size, num_layers).to(device),
    "rnnatm":RNNATM(input_size, hidden_size, num_layers).to(device),
    "cnnatm": CNNATM(input_size, hidden_size, num_layers).to(device),
    "cnnrnn": CNNRNN(input_size, hidden_size, num_layers).to(device),
    "cnnrnnatm":CNNRNNATM(input_size, hidden_size, num_layers).to(device),
    }

    model = models[args.model]

    tnet = Tripletnet(model)

    criterion = nn.TripletMarginLoss(margin=1.0, p=2)
    optimizer = optim.Adam(tnet.parameters(), lr=0.0001)

    if args.weights:
        load_checkpoint(tnet, optimizer, args.weights)

    n_parameters = sum([p.data.nelement() for p in tnet.parameters()])
    print('  + Number of params: {}'.format(n_parameters))

    for epoch in range(1, 50000 + 1):
        # train for one epoch
        train(train_loader, tnet, criterion, optimizer, epoch)
        # evaluate on validation set
        test(test_loader, tnet, criterion)
        state = {'epoch': epoch + 1, 'state_dict': tnet.state_dict(), 'optimizer': optimizer.state_dict() }
        torch.save(state, "gait.triplet.torch." + str(epoch))
