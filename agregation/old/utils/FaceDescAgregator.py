import numpy

class FaceDescAgregator:
    """docstring for FaceDescAgregator."""
    _vectors = []
    def __init__(self,name = "unknown file"):
        self.name = name

    def add(self,vector):
        self._vectors.append(FaceDescAgregator.dlibVect_to_numpyNDArray(vector))

    #def agregate(self):
    #    mean = numpy.mean(self._vectors,axis=0)
    #    return mean
    def agregate(self):
        return self._vectors[int(len(self._vectors)/2)]

    @staticmethod
    def dlibVect_to_numpyNDArray(vector):
        array = numpy.zeros(shape=128)
        for i in range(0, len(vector)):
            array[i] = vector[i]
        return array
