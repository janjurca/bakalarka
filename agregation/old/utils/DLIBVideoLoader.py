import cv2
from enum import Enum

class Compatibility(Enum):
    DLIB_COMPATIBLE = "DLIB_COMPATIBLE"


class DLIBVideoLoader:
    _actual_frame = 0
    _cap = None
    """docstring for dlib_video_loader."""
    def __init__(self, path):
        self.path = path
        self._cap = cv2.VideoCapture(path)

    def available(self):
        return self._cap.isOpened()

    def next(self,param):
        if self.available():
            self._actual_frame = self._actual_frame + 1
            _, frame = self._cap.read()
            if frame is None:
                raise Exception("Error while reading frame")
            if param == Compatibility.DLIB_COMPATIBLE:
                frame =  cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            return frame
        else:
            raise Exception("Capture is out of frame")

    def frame_index(self):
        return self._actual_frame

    def release(self):
        self._cap.release()
