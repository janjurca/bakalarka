import torch
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms
import torch.nn.functional as F
import torch
import torch.nn
import random
from torch.utils.data import DataLoader,Dataset
import pickle
import numpy as np
from torch import optim
import matplotlib.pyplot as plt
import os
import argparse
from siameseDataset import SiameseNetworkDataset
# Device configuration
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

def dprint(*args, **kwargs):
    print( " ".join(map(str,args)), **kwargs)
    pass

class ContrastiveLoss(torch.nn.Module):
    """
    Contrastive loss function.
    Based on: http://yann.lecun.com/exdb/publis/pdf/hadsell-chopra-lecun-06.pdf
    """

    def __init__(self, margin=2):
        super(ContrastiveLoss, self).__init__()
        self.margin = margin


    def forward(self, output1, output2, label):
        euclidean_distance = F.pairwise_distance(output1, output2)
        loss_contrastive = torch.mean((1-label) * torch.pow(euclidean_distance, 2) + (label) * torch.pow(torch.clamp(self.margin - euclidean_distance, min=0.0), 2))
        return loss_contrastive



# Fully connected neural network with one hidden layer
class NeuralNet(nn.Module):
    def __init__(self):
        super(NeuralNet, self).__init__()
        # kernel

        self.weight0 = torch.randn(128,1)

        self.q0 = torch.nn.Parameter(self.weight0)
        self.fc1 = nn.Linear(128, 128)

    def forward_once(self, X):
        #dprint("Q0:",self.q0.shape)
        #dprint("Input X:", X.shape)
        o = torch.matmul(X,self.q0)
        #dprint("Vahy O:",o.shape)
        o = F.softmax(o, dim=1)
        #dprint("Softmax vah",o.shape)
        #dprint(X[0])
        #dprint((X*o)[0])
        #dprint(o[0])
        #dprint("Mean",(X*o).mean(dim=-2).shape)
        #FIXME not mean but sum
        o = torch.tanh(self.fc1((X*o).sum(dim=-2)))
        #dprint("R0",o.unsqueeze(1).transpose(dim0=-3,dim1=-2).shape)
        #dprint("R0",o.unsqueeze(2).shape)
        #dprint("X",X.shape)
        o = torch.matmul(X,o.unsqueeze(2))
        o = F.softmax(o,dim=1)
        #print("Finalni vahy",o)
        o = (X*o).sum(dim=-2)
        #dprint("Final:",o)
        return o

    def forward(self, input1, input2):
        output1 = self.forward_once(input1)
        output2 = self.forward_once(input2)
        return output1, output2
