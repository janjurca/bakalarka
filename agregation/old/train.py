from network import *
from siameseDataset import *
import torch
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms
import torch.nn.functional as F
import torch
import torch.nn
import numpy as np

dataset = SiameseNetworkDataset("/home/rmin/Dokumenty/extracted/transposed.pic","/home/rmin/Dokumenty/extracted/same_indexes.pic","/home/rmin/Dokumenty/extracted/diff_indexes.pic")


net = NeuralNet().to(device)
if os.path.isfile('./network.torch'):
    print("Loading model form file")
    net.load_state_dict(torch.load("./network.torch"))


criterion = ContrastiveLoss()
optimizer = optim.RMSprop(net.parameters(),lr = 0.001 )


counter = []
loss_history = []
iteration_number= 0
increment = 5000
start = 0
end = increment

for epoch in range(0,10):
    optimizer.zero_grad()
    first = []
    second = []
    labels = []
    for i in range(start,end):
        t0, t1 , label = dataset[i]
        needed_len = 30
        if len(t0.tolist()) < needed_len or len(t1.tolist()) < needed_len:
            continue
        first.append(t0.tolist()[:needed_len])
        second.append(t1.tolist()[:needed_len])
        labels.append(label.tolist())
        #if i% 1000 == 0:
        #print("Input vector",i)
    start = end
    end = end + increment
    if end >= len(dataset):
        start = 0
        end = increment

    t0 = torch.tensor(first)
    t1 = torch.tensor(second)
    labels = torch.tensor(labels)

    output1,output2 = net(t0.float(),t1.float())

    loss_contrastive = criterion(output1,output2,labels)
    loss_contrastive.backward()
    optimizer.step()

    print("Epoch number {}\n Current loss {}\n".format(epoch,loss_contrastive.item()))
    iteration_number += 1
    counter.append(iteration_number)
    loss_history.append(loss_contrastive.item())

torch.save(net.state_dict(), "network.torch")


plt.plot(loss_history)
plt.show()
