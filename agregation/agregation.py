import pickle
import numpy as np


def agregate_track(track, method = "mean"):
    '''
    track = np.array([
    [vector for one frame],
    [vector for next frame],
    ])
    '''
    if method == "mean":
        return np.mean(track, axis=0)
    elif method == "net":
        return None
