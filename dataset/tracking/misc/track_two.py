import argparse
import logging
import sys
import time
import glob

from tf_pose import common
import cv2
import numpy as np
from tf_pose.estimator import TfPoseEstimator
from tf_pose.networks import get_graph_path, model_wh
import pickle
import numpy
from scipy.spatial import distance

logger = logging.getLogger('TfPoseEstimator')
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('[%(asctime)s] [%(name)s] [%(levelname)s] %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


class Color(object):
    c = [
    [255,255,255],
    [255,0,255],
    [255,255,0],
    [255,0,0],
    [0,255,255],
    [0,0,255],
    [128,128,128],
    [128,0,128],
    [128,128,0],
    [128,0,0],
    [0,128,128],
    [0,0,128],
    ]

    def __getitem__(self, key):
        return self.c[key%len(self.c)]

persons = []
'''
persons = [
[{'frame':42,'pose':posedata}],

]
'''

all_poses = []
'''
all_poses = [
[{'id':id, 'pose':posedata},] #frame
]

'''

'''

last:
[{'id':1, 'pose':posedata},{'id':2, 'pose':posedata},{'id':3, 'pose':posedata}]



curent:
[Human,Human]



'''
image_w = 0
image_h = 0

def get_neck(pose):
    return (int(pose.body_parts[1].x * image_w + 0.5), int(pose.body_parts[1].y * image_h + 0.5))


def new_person(frame,pose):
    persons.append([{"frame":frame,'pose':pose}])
    return len(persons)-1

def add_person_pose(id,frame,pose):
    persons[id].append({"frame":frame,'pose':pose})

def del_col(matrix,col):
    ret = []
    for row in matrix:
        r = []
        for i,c in enumerate(row):
            if i != col:
                r.append(c)
        ret.append(r)

    return ret

def blacklisted_min(dists_row, blacklist):
    min = 9999
    index = None
    for i, item in enumerate(dists_row):
        if i in blacklist:
            continue
        if item < min:
            min = item
            index = i

    return min, index

def fill_list(n):
    l = []
    for i in range(n):
        l.append(i)
    return l

def get_curent_mapping(frame,new_poses,current_poses):
    distances = []
    '''
    distance = [
    [new1 new2] current1
    [new1 new2] current2
    ]
    '''

    for current_pose_data in current_poses:
        current_pose =  current_pose_data['pose']
        current_neck = get_neck(current_pose)
        d = []
        for new_pose in new_poses:
            new_neck = get_neck(new_pose)
            dist = distance.euclidean(current_neck,new_neck)
            d.append(dist)
        distances.append(d)
    print(numpy.array(distances),len(distances),len(current_poses),len(new_poses))
    new_map_pairs = []
    l = len(distances)

    blacklist_cols = []
    for i in range(l):
        dists = distances[i]

        min,new_index = blacklisted_min(dists,blacklist_cols)
        if new_index == None:
            break
        if min < 60:
            current_id = current_poses[i]['id']
            new_map_pairs.append({'id':current_id, 'pose':new_poses[new_index]})

            blacklist_cols.append(new_index)

    unused_indexes =  [x for x in fill_list(len(new_poses)) if x not in blacklist_cols]
    for index in unused_indexes:
        id = new_person(frame,new_poses[index])
        new_map_pairs.append({'id':id, 'pose':new_poses[index]})

    #print("NEW-----------",new_map_pairs)
    return new_map_pairs

def has_neck(pose):
    try:
        get_neck(pose)
        return True
    except Exception as e:
        return False

def process_frame(index,poses):
    filltered = []
    for pose in poses:
        if has_neck(pose):
            filltered.append(pose)
    poses = filltered
    if len(all_poses) == 0:
        current_frame_poses = []
        for pose in poses:
            id = new_person(index,pose)
            current_frame_poses.append({'id':id,'pose':pose})
        all_poses.append(current_frame_poses)
    else:
        new_map = get_curent_mapping(index,poses,all_poses[-1])
        all_poses.append(new_map)





parser = argparse.ArgumentParser(description='tf-pose-estimation run here')
parser.add_argument('--overview', type=str, default='/run/media/jjurca/17010F223A137D4C/BUT_pedestrians/S1000001.MP4')
parser.add_argument('--entry', type=str, default='/run/media/jjurca/17010F223A137D4C/BUT_pedestrians/S1020002.MP4')
parser.add_argument('--d105', type=str, default='/run/media/jjurca/17010F223A137D4C/BUT_pedestrians/S1110001.MP4')
parser.add_argument('--e112', type=str, default='/run/media/jjurca/17010F223A137D4C/BUT_pedestrians/S1090001.MP4')

parser.add_argument('--overview_pose', type=str, default='/home/rmin/Dokumenty/extracted/pose/S1000001.MP4.pose')
parser.add_argument('--entry_pose', type=str, default='/home/rmin/Dokumenty/extracted/pose/S1020002.MP4.pose')
parser.add_argument('--d105_pose', type=str, default='/home/rmin/Dokumenty/extracted/pose/S1110001.MP4.pose')
parser.add_argument('--e112_pose', type=str, default='/home/rmin/Dokumenty/extracted/pose/S1090001.MP4.pose')

parser.add_argument('--ui', type=bool, default=False, help='pickled file output')

parser.add_argument('--resize', type=str, default='432x368',
                    help='if provided, resize images before they are processed. default=0x0, Recommends : 432x368 or 656x368 or 1312x736 ')
parser.add_argument('--resize-out-ratio', type=float, default=4.0,
                    help='if provided, resize heatmaps before they are post-processed. default=1.0')

args = parser.parse_args()

w, h = model_wh(args.resize)


cap_overview = cv2.VideoCapture(args.overview)
cap_e112 = cv2.VideoCapture(args.e112)


if not cap_overview.isOpened():
    print("ERROR openning cap_overview", args.overview)
    exit(0)
if not cap_e112.isOpened():
    print("ERROR openning cap_e112", args.e112)
    exit(0)


poses_overview = pickle.load(open(args.overview_pose,'rb'))
poses_e112 = pickle.load(open(args.e112_pose,'rb'))

#####################SYNC frames#########################################š
#ENTRY: S1020002.MP4	3559(146.36s)	== 	3286(131,4s) delta = 273
#E112:  S1090001.MP4	6008(240,32s)	== 	3038(121,5s) delta = 2970
#D105:  S1110001.MP4	8145(325,8s)	== 	3038(121,5s) delta = 5107
#overview S1000001.MP4


#cap_overview.set(1,3038)
#cap_entry.set(1,3311)
#cap_d105.set(1,8145)
#cap_e112.set(1,6008)

print("STATUS cap_overview",cap_overview.set(cv2.CAP_PROP_POS_MSEC,39000))
print("STATUS cap_e112",cap_e112.set(cv2.CAP_PROP_POS_MSEC,218000))


#for i in range(3038):
#    cap_overview.read()
#for i in range(3311):
#    cap_entry.read()
#for i in range(8145):
#    cap_d105.read()
#for i in range(6008):
#    cap_e112.read()

print("SET")
while cap_overview.isOpened() and cap_e112.isOpened():
    _, im_overview = cap_overview.read()
    _, im_e112 = cap_e112.read()

    im_overview =  cv2.resize(im_overview,(640,360))
    im_e112 =  cv2.resize(im_e112,(640,360))

    screen = np.concatenate((im_overview, im_e112), axis=1)

    cv2.imshow('Numpy Horizontal Concat', screen)

    cv2.waitKey(0)

exit(0)

for i, poses in enumerate(poses_list):
    if cap.isOpened():
        ret_val, image = cap.read()
        #if i < 1200:
        #    print(i)
        #    continue

        process_frame(i,poses)
        for pose_data in all_poses[-1]:
            id = pose_data['id']
            pose = pose_data['pose']

            # indexes
            # https://github.com/CMU-Perceptual-Computing-Lab/openpose/blob/master/doc/output.md#keypoint-ordering
            image_h, image_w = image.shape[:2]
            body_part = pose.body_parts[1]
            color = Color()[id]
            neck = (int(body_part.x * image_w + 0.5), int(body_part.y * image_h + 0.5))
            cv2.circle(image, neck, 10, color, thickness=20, lineType=8, shift=0)


        if args.ui:
            image = cv2.resize(image, (640,360))                    # Resize image
            cv2.imshow('tf-pose-estimation result', image)
            cv2.waitKey(0)
            #if cv2.waitKey(1) & 0xFF == ord('q'):
            #    break
