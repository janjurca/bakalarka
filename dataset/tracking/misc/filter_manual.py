import glob
import argparse
import os
import shutil
import cv2
import numpy
import pickle
parser = argparse.ArgumentParser(description='tf-pose-estimation run here')
parser.add_argument('--folder', action='store', default=None, help='folder')
parser.add_argument('--overview', action='store', default="h2", help='folder')

args = parser.parse_args()

def removeEmptyFolders(path, removeRoot=True):
    'Function to remove empty folders'
    if not os.path.isdir(path):
        return

    # remove empty subfolders
    files = os.listdir(path)
    if len(files):
        for f in files:
            fullpath = os.path.join(path, f)
            if os.path.isdir(fullpath):
                removeEmptyFolders(fullpath)

    # if folder empty, delete it
    files = os.listdir(path)
    if len(files) == 0 and removeRoot:
        print ("Removing empty folder:", path)
        os.rmdir(path)

##################remove short tracks
def remove_short_tracks():
    files = os.listdir(args.folder)
    for name in files:
        sub = os.listdir(args.folder.rstrip("/") + "/" + name)
        for camera in sub:
            sub = os.listdir(args.folder.rstrip("/") + "/" + name + "/" + camera)
            for track in sub:
                sub = os.listdir(args.folder.rstrip("/") + "/" + name + "/" + camera + "/" + track)
                if len(sub) < 75:
                    print(len(sub),args.folder.rstrip("/") + "/" + name + "/" + camera + "/" + track)
                    shutil.rmtree(args.folder.rstrip("/") + "/" + name + "/" + camera + "/" + track)


##################remove short tracks
def check_bad_clones_persons():
    bad_cams = []
    files = os.listdir(args.folder)
    for name in files:
        sub = os.listdir(args.folder.rstrip("/") + "/" + name)
        for camera in sub:
            sub = os.listdir(args.folder.rstrip("/") + "/" + name + "/" + camera)
            if len(sub) > 1:
                print("WARNING", args.folder.rstrip("/") + "/" + name + "/" + camera)
                bad_cams.append(args.folder.rstrip("/") + "/" + name + "/" + camera)
    return bad_cams


#################REMOVE peoples just from one camera
def remove_one_camera_tracks():
    files = os.listdir(args.folder)
    for name in files:
        sub = os.listdir(args.folder.rstrip("/") + "/" + name)
        if len(sub) == 1:
            print(name,sub)
            shutil.rmtree(args.folder.rstrip("/") + "/" + name)

def user_select(cam_folder):
    sub = os.listdir(cam_folder)
    images = []
    for track in sub:
        images.append(list(glob.glob(cam_folder.rstrip("/") +"/" + track + "/"+ "*.jpg"))[0])

    loaded_images = []
    for image in images:
        print(image)
        loaded_images.append(cv2.resize(cv2.imread(image),(128, 256)))

    screen = numpy.concatenate(loaded_images,axis=1)

    print("Bad cam", cam_folder)
    l = list(glob.glob(cam_folder +"/" + "../"+args.overview+"/*/*.jpg"))

    o_im = cv2.imread(l[int(len(l)/2)])
    cv2.imshow('Overview', o_im)
    cv2.imshow('Numpy Horizontal Concat', screen)
    key = cv2.waitKey(0)
    number = key - 48
    if number >=0 and number <=9:
        print("selected index is ",number)
        sub.remove(sub[number])
        return sub
    elif number == -35:
        print("All bad ",number)

        return sub
    else:
        return None


#remove_short_tracks()
#removeEmptyFolders(args.folder)
#remove_one_camera_tracks()
bad_cams = check_bad_clones_persons()

bad_tracks = []
for i, bad in enumerate(bad_cams):
    try:
        b = user_select(bad)
    except:
        continue
    print(i,b)
    if b:
        bad_tracks.extend(b)
    pickle.dump(bad_tracks,open('bad_trakcs.pic','wb'))
