import glob
import argparse
import os
import shutil
import cv2
import numpy
import pickle
import glob
import shutil


parser = argparse.ArgumentParser(description='tf-pose-estimation run')
parser.add_argument('--folder', action='store', default=None, help='folder')
parser.add_argument('--file', action='store', default=None, help='file')

args = parser.parse_args()

def get_tracks(root):
    ret = []
    for dirpath, dirnames, filenames in os.walk(root):
        if not dirnames:
            ret.append(dirpath)
    return ret



def show_folders(folders):
    loaded_images = []
    for folder in folders:
        image = glob.glob(folder + "/*.jpg")
        image = image[int(len(image)/2)]
        loaded_images.append(cv2.resize(cv2.imread(image),(128, 256)))

    screen = numpy.concatenate(loaded_images,axis=1)
    cv2.imshow('images', screen)
    key = cv2.waitKey(0)

    number = key
    print(number)
    if number == 13 :
        print("True")
        return True
    elif number == 27:
        print("False")
        return False


def select_bad(folders):
    loaded_images = []
    for folder in folders:
        image = glob.glob(folder + "/*.jpg")
        image = image[int(len(image)/2)]
        loaded_images.append(cv2.resize(cv2.imread(image),(128, 256)))

    screen = numpy.concatenate(loaded_images,axis=1)
    cv2.imshow('images', screen)
    key = cv2.waitKey(0)

    number = key
    return number

if args.folder and not args.file:

    bad = []
    for folder in glob.glob(args.folder + "/*"):

        tracks = get_tracks(folder)
        if not show_folders(tracks):
            bad.append(folder)
        pickle.dump(bad,open("bad_persons.pic",'wb'))

elif not args.folder and args.file:
    final = []
    folders = pickle.load(open(args.file,'rb'))
    print("Count",len(folders))
    for i,folder in enumerate(folders):
        print(i,"/",len(folders),folder)
        tracks = get_tracks(folder)
        try:
            bad_track = select_bad(tracks)
        except Exception as e:
            continue
        if bad_track == 27:
            print("Removing", folder)
            shutil.rmtree(folder)
        else:
            final.append(folder)
    pickle.dump(final,open('final_to.pic','wb'))
