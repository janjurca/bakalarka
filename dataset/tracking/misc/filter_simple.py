import glob
import argparse
import os
import shutil

parser = argparse.ArgumentParser(description='tf-pose-estimation run here')
parser.add_argument('--folder', action='store', default=None, help='folder')

args = parser.parse_args()

def removeEmptyFolders(path, removeRoot=True):
    'Function to remove empty folders'
    if not os.path.isdir(path):
        return

    # remove empty subfolders
    files = os.listdir(path)
    if len(files):
        for f in files:
            fullpath = os.path.join(path, f)
            if os.path.isdir(fullpath):
                removeEmptyFolders(fullpath)

    # if folder empty, delete it
    files = os.listdir(path)
    if len(files) == 0 and removeRoot:
        print ("Removing empty folder:", path)
        os.rmdir(path)

##################remove short tracks
def remove_short_tracks():
    files = os.listdir(args.folder)
    for name in files:
        sub = os.listdir(args.folder.rstrip("/") + "/" + name)
        for camera in sub:
            sub = os.listdir(args.folder.rstrip("/") + "/" + name + "/" + camera)
            for track in sub:
                sub = os.listdir(args.folder.rstrip("/") + "/" + name + "/" + camera + "/" + track)
                if len(sub) < 75:
                    print(len(sub),args.folder.rstrip("/") + "/" + name + "/" + camera + "/" + track)
                    shutil.rmtree(args.folder.rstrip("/") + "/" + name + "/" + camera + "/" + track)


##################remove short tracks
def check_bad_clones_persons():
    files = os.listdir(args.folder)
    for name in files:
        sub = os.listdir(args.folder.rstrip("/") + "/" + name)
        for camera in sub:
            sub = os.listdir(args.folder.rstrip("/") + "/" + name + "/" + camera)
            if len(sub) > 1:
                print("WARNING", args.folder.rstrip("/") + "/" + name + "/" + camera)


#################REMOVE peoples just from one camera
def remove_one_camera_tracks():
    files = os.listdir(args.folder)
    for name in files:
        sub = os.listdir(args.folder.rstrip("/") + "/" + name)
        if len(sub) == 1:
            print(name,sub)
            shutil.rmtree(args.folder.rstrip("/") + "/" + name)

def is_track_continuous(trackdir):
    images = list(glob.glob(trackdir + "/*jpg"))
    images.sort()
    diff = int(images[-1].split("/")[-1].split(".")[0]) - int(images[0].split("/")[-1].split(".")[0])
    s = len(images)*0.2
    if abs(diff-len(images)) >s:
        print(images[0].rsplit("/",1)[0],diff,len(images))
        return False
    return True

def get_tracks(root):
    dirs = []
    for root, directories, filenames in os.walk(root):
        for directory in directories:
            if "entry" in root or "overview" in root or "e112" in root or "d105" in root:
                dirs.append(os.path.join(root, directory))
    return dirs

remove_short_tracks()

tracks = get_tracks(args.folder)
for track in tracks:
    if is_track_continuous(track) == False:
        shutil.rmtree(track)

removeEmptyFolders(args.folder)
remove_one_camera_tracks()
check_bad_clones_persons()
