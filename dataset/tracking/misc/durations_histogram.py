import glob
import argparse
import os
import shutil
import numpy as np
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description='tf-pose-estimation run')
parser.add_argument('--folder', action='store',  help='folder')

args = parser.parse_args()

##################remove short tracks
def get_tracks_lens():
    lens = []
    files = os.listdir(args.folder)
    for name in files:
        sub = os.listdir(args.folder.rstrip("/") + "/" + name)
        for camera in sub:
            sub = os.listdir(args.folder.rstrip("/") + "/" + name + "/" + camera)
            for track in sub:
                sub = os.listdir(args.folder.rstrip("/") + "/" + name + "/" + camera + "/" + track)
                lens.append(len(sub)/2)
                if len(sub) > 2000:
                    print (args.folder.rstrip("/") + "/" + name + "/" + camera + "/" + track)
    return lens

lens = get_tracks_lens()
hist, bin_edges = np.histogram(lens,bins=100)
plt.hist(lens, bins=bin_edges)  # arguments are passed to np.histogram
plt.xlabel("délka sekvence [s]")
plt.ylabel("počet sekvencí")

#plt.hist(lens, bins='auto')  # arguments are passed to np.histogram
plt.title("Histogram délek jednotlivých sekvencí")
plt.show()
