import cv2
import pickle
import numpy as np
import glob
import argparse
import os
import shutil

def get_hip(pose):
    try:
        return pose[8]
    except Exception as e:
        pass
    try:
        return pose[11]
    except Exception as e:
        pass
    raise Exception("Nohip")

parser = argparse.ArgumentParser(description='')
parser.add_argument('--folder', action='store', default=None, help='folder')
parser.add_argument('--file', action='store', default=None, help='file')
parser.add_argument('--person', action='store', default=None, help='file')

args = parser.parse_args()

if args.folder:
    poses = glob.glob(args.folder.rstrip("/") + "/*.pose")
    poses.sort()

    height = 512
    width = 256

    shift = np.array([128,70])
    scale = 150
    for pose_file in poses:
        pose = pickle.load(open(pose_file,'rb'))
        blank_image = np.zeros((height,width,3), np.uint8)
        neck = np.array(pose[1])
        hip = np.array(get_hip(pose))
        body_norm = np.linalg.norm(neck-hip)

        for point in pose:
            point = np.array(point)

            #point = point/body_norm


            point = point*scale
            point = point + shift
            x, y = int(point[0]), int(point[1])
            print("======",x,y)
            cv2.circle(blank_image, (x,y), 1, (255,255,255), thickness=1)
        cv2.imshow("image",blank_image)
        cv2.waitKey(10000)

elif args.file:
    poses = pickle.load(open(args.file,'rb'))
    files = list(poses.keys())
    files.sort()
    height = 1024
    width = 512

    shift = np.array([256,90])
    scale = 150

    for file in files:
        pose = poses[file]
        blank_image = np.zeros((height,width,3), np.uint8)
        neck = np.array(pose[1])
        hip = np.array(get_hip(pose))
        body_norm = np.linalg.norm(neck-hip)

        for point in pose:
            point = np.array(point)

            #point = point/body_norm
            point = point*scale
            point = point + shift
            x, y = int(point[0]), int(point[1])
            print("======",x,y)
            cv2.circle(blank_image, (x,y), 1, (255,255,255), thickness=1)
        cv2.imshow("image",blank_image)
        cv2.waitKey(5000)
elif args.person:
    poses = pickle.load(open(args.person,'rb'))
    height = 1024
    width = 512

    shift = np.array([256,90])
    scale = 150
    for person_id in poses:
        for cam in poses[person_id]:
            for frame in poses[person_id][cam]:
                pose = frame
                blank_image = np.zeros((height,width,3), np.uint8)
                neck = np.array(pose[1])
                hip = np.array(get_hip(pose))
                body_norm = np.linalg.norm(neck-hip)

                for point in pose:
                    point = np.array(point)

                    #point = point/body_norm
                    point = point*scale
                    point = point + shift
                    x, y = int(point[0]), int(point[1])
                    print("======",x,y)
                    cv2.circle(blank_image, (x,y), 1, (255,255,255), thickness=1)
                cv2.imshow("image",blank_image)
                cv2.waitKey(5000)
