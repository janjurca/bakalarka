import argparse
import logging
import sys
import time
import glob
import os
import errno

from tf_pose import common
import cv2
import numpy as np
from tf_pose.estimator import TfPoseEstimator
from tf_pose.networks import get_graph_path, model_wh
import pickle
import numpy
from scipy.spatial import distance
from numpy.linalg import inv


class Color(object):
    c = [
    [255,255,255],
    [255,0,255],
    [255,255,0],
    [0,255,255],
    [255,0,0],
    [0,255,0],
    [0,0,255],
    ]

    def __getitem__(self, key):
        return self.c[key%len(self.c)]

persons = []
'''
!!!!!!!!!!!!!!!
posedata = [Bodypart_from_tf_openpose,Bodypart_from_tf_openpose...]

persons = [
[{'frame':42,'pose':posedata}],

]
'''

all_poses = []
'''
all_poses = [
[{'id':id, 'pose':posedata, 'coords':(x,y)},] #frame
]

'''

'''

last:
[{'id':1, 'pose':posedata},{'id':2, 'pose':posedata},{'id':3, 'pose':posedata}]



curent:
[Human,Human]

'''

'''
tracks merging in this structure

tracks_dists = {
    'overview_id':{
        'cam_track_id':
            [
                list of all distance within frames
            ]
    }
}

'''
tresh = 40
image_w = 3840
image_h = 2160

def filter_tracks(cap_track):
    ids_count = {}
    for frame in cap_track:
        for human in frame:
            if human['id'] in ids_count.keys():
                ids_count[human['id']] = ids_count[human['id']] + 1
            else:
                ids_count[human['id']] = 1

    ###############MARK short tracks persons
    to_remove =  []
    for key in ids_count.keys():
        if ids_count[key] < 75:
            to_remove.append(int(key))

    filterred = []
    for frame in cap_track:
        fr = []
        for human in frame:
            if human['id'] not in to_remove:
                fr.append(human)
        filterred.append(fr)

    return filterred

def coords_to_numpy(list_of_coords):
    r = []
    for coords in list_of_coords:
        r.append([coords[0],coords[1]])
    return np.array(r)

def compute_distances(camera1_tracks,camera2_tracks,H_c1_to_c2,scale_up,scale_down):
    '''
    tracks_dists = {
        'cam1_human_id':{
            'cam2_human_id':
                [
                    list of all distance within frames
                ]
        }
    }
    '''
    c1_tracks_lens = {}
    c2_tracks_lens = {}

    for c1_frame,c2_frame in zip(camera1_tracks,camera2_tracks):
        for c1_human in c1_frame:
            if c1_human['id'] not in c1_tracks_lens.keys():
                c1_tracks_lens[c1_human['id']] = 1
            else:
                c1_tracks_lens[c1_human['id']] = c1_tracks_lens[c1_human['id']] + 1

        for c2_human in c2_frame:
            if c2_human['id'] not in c2_tracks_lens.keys():
                c2_tracks_lens[c2_human['id']] = 1
            else:
                c2_tracks_lens[c2_human['id']] = c2_tracks_lens[c2_human['id']] + 1


    tracks_dists = {}
    for c1_frame,c2_frame in zip(camera1_tracks,camera2_tracks):

        for c1_human in c1_frame:
            if c1_human['id'] not in tracks_dists.keys():
                tracks_dists[c1_human['id']] = {}
                c1_coords = c1_human['coords']
                p = np.array([[[c1_coords[0]*scale_up,c1_coords[1]*scale_up+10]]],dtype='float32')
                pointsOut = cv2.perspectiveTransform(p, H_c1_to_c2)
                x = int(pointsOut[0][0][0]/scale_down)
                y = int(pointsOut[0][0][1]/scale_down)

            for c2_human in c2_frame:
                if c2_human['id'] not in tracks_dists[c1_human['id']].keys():
                    tracks_dists[c1_human['id']][c2_human['id']] = []

                c2_coords = c2_human['coords']

                tracks_dists[c1_human['id']][c2_human['id']].append(distance.euclidean((x,y),c2_coords))

    averaged_dists = {}
    for c1_human_id in tracks_dists.keys():
        averaged_dists[c1_human_id] = {}
        for c2_human_id in tracks_dists[c1_human_id].keys():
            #if len(tracks_dists[c1_human_id][c2_human_id]) > 25:
            averaged_dists[c1_human_id][c2_human_id] = np.mean(np.array(tracks_dists[c1_human_id][c2_human_id]))


    return averaged_dists , tracks_dists, c1_tracks_lens, c2_tracks_lens



def pair_id_of_humans(dists_matrix):
    '''
    pairs = {id2:{'id':id1,'dist':distance}
    '''
    pairs = {}
    for c1_id in dists_matrix.keys():
        min_dist = 99999
        min_dist_id = None
        for c2_id in dists_matrix[c1_id].keys():
            if dists_matrix[c1_id][c2_id] < min_dist:
                min_dist = dists_matrix[c1_id][c2_id]
                min_dist_id = c2_id
        if min_dist_id:
            pairs[min_dist_id] = {'id':c1_id,'dist':min_dist}

    return pairs


def draw_to_image_mapped(poses, H, image,scale_up,scale_down):
    for pose_data in poses:
        id = pose_data['id']
        pose = pose_data['pose']
        coords = pose_data['coords']
        # indexes
        # https://github.com/CMU-Perceptual-Computing-Lab/openpose/blob/master/doc/output.md#keypoint-ordering
        color = Color()[id]
        p = np.array([[[coords[0]*scale_up,coords[1]*scale_up+10]]],dtype='float32')

        pointsOut = cv2.perspectiveTransform(p, H)
        x = int(pointsOut[0][0][0]/scale_down)
        y = int(pointsOut[0][0][1]/scale_down)

        cv2.circle(image, (x, y), 10, color, thickness=4, lineType=8, shift=0)
    return image

def draw_poses_to_image(image,poses):
    for pose_data in poses:
        id = pose_data['id']
        pose = pose_data['pose']
        coords = pose_data['coords']
        # indexes
        # https://github.com/CMU-Perceptual-Computing-Lab/openpose/blob/master/doc/output.md#keypoint-ordering
        image_h, image_w = image.shape[:2]
        body_part = pose.body_parts[1]
        color = Color()[id]
        #print(id)
        neck = (int(body_part.x * image_w + 0.5), int(body_part.y * image_h + 0.5))
        cv2.circle(image, neck, 3, color, thickness=4, lineType=8, shift=0)
        cv2.circle(image, coords, 3, color, thickness=4, lineType=8, shift=0)
    return image

def draw_paired_poses_to_image(image,poses,pairs = None):
    for pose_data in poses:
        id = pose_data['id']
        pose = pose_data['pose']
        coords = pose_data['coords']
        # indexes
        # https://github.com/CMU-Perceptual-Computing-Lab/openpose/blob/master/doc/output.md#keypoint-ordering
        image_h, image_w = image.shape[:2]
        body_part = pose.body_parts[1]
        color = Color()[id]
        #print(id)
        neck = (int(body_part.x * image_w + 0.5), int(body_part.y * image_h + 0.5))
        cv2.circle(image, neck, 3, color, thickness=4, lineType=8, shift=0)
        cv2.circle(image, coords, 3, color, thickness=4, lineType=8, shift=0)

        if pairs:
            try:
                id = pairs[id]
                cv2.putText(image,str(id),neck, cv2.FONT_HERSHEY_SIMPLEX, 1,(255,255,255),2,cv2.LINE_AA)
            except Exception as e:
                pass
        else:
            cv2.putText(image,str(id),neck, cv2.FONT_HERSHEY_SIMPLEX, 1,(255,255,255),2,cv2.LINE_AA)

    return image

def get_neck(pose):
    return (int(pose.body_parts[1].x * image_w + 0.5), int(pose.body_parts[1].y * image_h + 0.5))

def get_still_foot_coord(new_pose,current_pose):
    current_1 = (int(current_pose.body_parts[10].x * image_w + 0.5), int(current_pose.body_parts[10].y * image_h + 0.5))
    current_2 = (int(current_pose.body_parts[13].x * image_w + 0.5), int(current_pose.body_parts[13].y * image_h + 0.5))
    new_1 = (int(new_pose.body_parts[10].x * image_w + 0.5), int(new_pose.body_parts[10].y * image_h + 0.5))
    new_2 = (int(new_pose.body_parts[13].x * image_w + 0.5), int(new_pose.body_parts[13].y * image_h + 0.5))

    c1_n1 = distance.euclidean(current_1,new_1)
    c2_n2 = distance.euclidean(current_2,new_2)

    c1_n2 = distance.euclidean(current_1,new_2)
    c2_n1 = distance.euclidean(current_2,new_1)


    if c1_n1 < c2_n2:
        return new_1
    else:
        return new_2

def get_lowest_point(pose):
    y_max = 0
    ret = None
    for i in range(common.CocoPart.Background.value):
        if i not in pose.body_parts.keys():
            continue

        body_part = pose.body_parts[i]
        if (body_part.y * image_h + 0.5) > y_max:
            ret = body_part
            y_max = (body_part.y * image_h + 0.5)

    return (int(ret.x * image_w + 0.5), int(ret.y * image_h + 0.5))

def get_most_down_point(pose):
    y_max = 0
    ret = None
    for i in range(common.CocoPart.Background.value):
        if i not in pose.body_parts.keys():
            continue

        body_part = pose.body_parts[i]
        if (body_part.y * image_h + 0.5) > y_max:
            ret = body_part
            y_max = (body_part.y * image_h + 0.5)

    return (int(ret.x * image_w + 0.5), int(ret.y * image_h + 0.5))

def get_most_top_point(pose):
    min = 99999
    ret = None
    for i in range(common.CocoPart.Background.value):
        if i not in pose.body_parts.keys():
            continue

        body_part = pose.body_parts[i]
        if (body_part.y * image_h + 0.5) < min:
            ret = body_part
            min = (body_part.y * image_h + 0.5)

    return (int(ret.x * image_w + 0.5), int(ret.y * image_h + 0.5))



def get_most_right_point(pose):
    max = 0
    ret = None
    for i in range(common.CocoPart.Background.value):
        if i not in pose.body_parts.keys():
            continue

        body_part = pose.body_parts[i]
        if (body_part.x * image_h + 0.5) > max:
            ret = body_part
            max = (body_part.x * image_h + 0.5)

    return (int(ret.x * image_w + 0.5), int(ret.y * image_h + 0.5))

def get_most_left_point(pose):
    min = 999999
    ret = None
    for i in range(common.CocoPart.Background.value):
        if i not in pose.body_parts.keys():
            continue

        body_part = pose.body_parts[i]
        if (body_part.x * image_h + 0.5) < min:
            ret = body_part
            min = (body_part.x * image_h + 0.5)

    return (int(ret.x * image_w + 0.5), int(ret.y * image_h + 0.5))


def new_person(frame,pose):
    persons.append([{"frame":frame,'pose':pose}])
    return len(persons)-1

def add_person_pose(id,frame,pose):
    persons[id].append({"frame":frame,'pose':pose})

def del_col(matrix,col):
    ret = []
    for row in matrix:
        r = []
        for i,c in enumerate(row):
            if i != col:
                r.append(c)
        ret.append(r)

    return ret

def blacklisted_min(dists_row, blacklist):
    min = 9999
    index = None
    for i, item in enumerate(dists_row):
        if i in blacklist:
            continue
        if item < min:
            min = item
            index = i

    return min, index

def fill_list(n):
    l = []
    for i in range(n):
        l.append(i)
    return l

def get_curent_mapping(frame,new_poses,current_poses):
    distances = []
    '''
    distance = [
    [new1 new2] current1
    [new1 new2] current2
    ]
    '''

    for current_pose_data in current_poses:
        current_pose =  current_pose_data['pose']
        current_neck = get_neck(current_pose)
        d = []
        for new_pose in new_poses:
            new_neck = get_neck(new_pose)
            #print(current_neck,new_neck)
            dist = distance.euclidean(current_neck,new_neck)
            d.append(dist)
        distances.append(d)
    #print(numpy.array(distances),len(distances),len(current_poses),len(new_poses))

    new_map_pairs = []
    l = len(distances)

    blacklist_cols = []
    for i in range(l):
        dists = distances[i]

        min,new_index = blacklisted_min(dists,blacklist_cols)
        if new_index == None:
            break
        if min < 25:
            current_id = current_poses[i]['id']
            new_map_pairs.append({'id':current_id, 'pose':new_poses[new_index],'coords':get_lowest_point(new_poses[new_index])})
            blacklist_cols.append(new_index)

    unused_indexes =  [x for x in fill_list(len(new_poses)) if x not in blacklist_cols]
    for index in unused_indexes:
        id = new_person(frame,new_poses[index])
        new_map_pairs.append({'id':id, 'pose':new_poses[index],'coords':get_lowest_point(new_poses[index])})

    #print("NEW-----------",new_map_pairs)
    return new_map_pairs

def has_neck(pose):
    try:
        get_neck(pose)
        return True
    except Exception as e:
        return False

def process_frame(index,poses):
    filltered = []
    for pose in poses:
        if has_neck(pose):
            filltered.append(pose)
    poses = filltered
    if len(all_poses) == 0:
        current_frame_poses = []
        for pose in poses:
            id = new_person(index,pose)
            lowest = get_lowest_point(pose)
            current_frame_poses.append({'id':id,'pose':pose,'coords':lowest})
        all_poses.append(current_frame_poses)
    else:
        new_map = get_curent_mapping(index,poses,all_poses[-1])
        all_poses.append(new_map)





parser = argparse.ArgumentParser(description='tf-pose-estimation run here tu')
parser.add_argument('--overview', type=str, default='/run/media/jjurca/17010F223A137D4C/dataset_sources/fit/overview.mp4')
parser.add_argument('--entry', type=str, default='/run/media/jjurca/17010F223A137D4C/dataset_sources/fit/entry.mp4')
parser.add_argument('--d105', type=str, default='/run/media/jjurca/17010F223A137D4C/dataset_sources/fit/d105.mp4')
parser.add_argument('--e112', type=str, default='/run/media/jjurca/17010F223A137D4C/dataset_sources/fit/e112.mp4')

parser.add_argument('--overview_pose', type=str, default='/run/media/jjurca/17010F223A137D4C/dataset_sources/fit/overview.mp4.pose')
parser.add_argument('--entry_pose', type=str, default='/run/media/jjurca/17010F223A137D4C/dataset_sources/fit/entry.mp4.pose')
parser.add_argument('--d105_pose', type=str, default='/run/media/jjurca/17010F223A137D4C/dataset_sources/fit/d105.mp4.pose')
parser.add_argument('--e112_pose', type=str, default='/run/media/jjurca/17010F223A137D4C/dataset_sources/fit/e112.mp4.pose')


parser.add_argument('--entry_homography', type=str, default='/run/media/jjurca/17010F223A137D4C/dataset_sources/fit/entry.npy')
parser.add_argument('--d105_homography', type=str, default='/run/media/jjurca/17010F223A137D4C/dataset_sources/fit/d105.npy')
parser.add_argument('--e112_homography', type=str, default='/run/media/jjurca/17010F223A137D4C/dataset_sources/fit/e112.npy')

parser.add_argument('--overview_tracks', type=str, default='/run/media/jjurca/17010F223A137D4C/dataset_sources/fit/overview_tracks.pic')
parser.add_argument('--entry_tracks', type=str, default='/run/media/jjurca/17010F223A137D4C/dataset_sources/fit/entry_tracks.pic')
parser.add_argument('--d105_tracks', type=str, default='/run/media/jjurca/17010F223A137D4C/dataset_sources/fit/d105_tracks.pic')
parser.add_argument('--e112_tracks', type=str, default='/run/media/jjurca/17010F223A137D4C/dataset_sources/fit/e112_tracks.pic')

parser.add_argument('--load_tracks',action='store_true',  default=False)
parser.add_argument('--ui', action='store_true', default=False, help='pickled file output')
parser.add_argument('--save', action='store', default=None, help='directory where to save cropped persons')

parser.add_argument('--resize', type=str, default='432x368',
                    help='if provided, resize images before they are processed. default=0x0, Recommends : 432x368 or 656x368 or 1312x736 ')

args = parser.parse_args()

w, h = model_wh(args.resize)


cap_overview = cv2.VideoCapture(args.overview)
cap_entry = cv2.VideoCapture(args.entry)
cap_d105 = cv2.VideoCapture(args.d105)
cap_e112 = cv2.VideoCapture(args.e112)


if not cap_overview.isOpened():
    print("ERROR openning cap_overview", args.overview)
    exit(0)
if not cap_entry.isOpened():
    print("ERROR openning cap_entry", args.entry)
    exit(0)
if not cap_d105.isOpened():
    print("ERROR openning cap_d105", args.d105)
    exit(0)
if not cap_e112.isOpened():
    print("ERROR openning cap_e112", args.e112)
    exit(0)


poses_overview = pickle.load(open(args.overview_pose,'rb'))
poses_entry = pickle.load(open(args.entry_pose,'rb'))
poses_d105 = pickle.load(open(args.d105_pose,'rb'))
poses_e112 = pickle.load(open(args.e112_pose,'rb'))

homography_entry_to_overview = np.load(args.entry_homography)
homography_d105_to_overview = np.load(args.d105_homography)
homography_e112_to_overview = np.load(args.e112_homography)

homography_overview_to_entry = inv(homography_entry_to_overview)
homography_overview_to_d105 = inv(homography_d105_to_overview)
homography_overview_to_e112 = inv(homography_e112_to_overview)


if args.load_tracks:
    overview_tracks = pickle.load(open(args.overview_tracks,'rb'))
    entry_tracks = pickle.load(open(args.entry_tracks,'rb'))
    d105_tracks = pickle.load(open(args.d105_tracks,'rb'))
    e112_tracks = pickle.load(open(args.e112_tracks,'rb'))
else:
    overview_tracks = []
    entry_tracks = []
    d105_tracks = []
    e112_tracks = []


    for i, poses in enumerate(poses_overview):
        process_frame(i,poses)
    overview_tracks = all_poses[:]
    print("overview_tracks PROCCESSED")
    all_poses = []
    for i, poses in enumerate(poses_entry):
        process_frame(i,poses)
    entry_tracks = all_poses[:]
    print("entry_tracks PROCCESSED")
    all_poses = []
    for i, poses in enumerate(poses_d105):
        process_frame(i,poses)
    d105_tracks = all_poses[:]
    print("d105_tracks PROCCESSED")
    all_poses = []
    for i, poses in enumerate(poses_e112):
        process_frame(i,poses)
    e112_tracks = all_poses[:]
    print("e112_tracks PROCCESSED")
    all_poses = []

    overview_tracks = filter_tracks(overview_tracks)
    print("overview_tracks FILTERRED")
    entry_tracks = filter_tracks(entry_tracks)
    print("entry_tracks FILTERRED")
    d105_tracks = filter_tracks(d105_tracks)
    print("d105_tracks FILTERRED")
    e112_tracks = filter_tracks(e112_tracks)
    print("e112_tracks FILTERRED")

    pickle.dump(overview_tracks, open(args.overview_tracks,'wb'))
    pickle.dump(entry_tracks, open(args.entry_tracks,'wb'))
    pickle.dump(d105_tracks, open(args.d105_tracks,'wb'))
    pickle.dump(e112_tracks, open(args.e112_tracks,'wb'))


#overview_to_entry_dists, overview_to_entry_dists_all, overview_to_entry_c1_tracks_lens, overview_to_entry_c2_tracks_lens = compute_distances(entry_tracks, overview_tracks, homography_entry_to_overview, 2, 1)
#print("overview_tracks distanced ")
#overview_to_d105_dists, overview_to_d105_dists_all, overview_to_d105_c1_tracks_lens, overview_to_d105_c2_tracks_lens = compute_distances(d105_tracks, overview_tracks, homography_d105_to_overview, 2, 2)
#print("entry_tracks distanced")
#overview_to_e112_dists, overview_to_e112_dists_all, overview_to_e112_c1_tracks_lens, overview_to_e112_c2_tracks_lens = compute_distances(e112_tracks, overview_tracks, homography_e112_to_overview, 2, 2)
#print("d105_tracks distanced ")
#
#
#entry_to_overview_pairs = pair_id_of_humans(overview_to_entry_dists)
#print("overview_tracks PAIRED")
#d105_to_overview_pairs = pair_id_of_humans(overview_to_d105_dists)
#print("entry_tracks PAIRED")
#e112_to_overview_pairs = pair_id_of_humans(overview_to_e112_dists)
#print("d105_tracks PAIRED")

def draw_closest(src_poses,dst_poses,dst_img,H,scale_up,scale_down):
    for src_human in src_poses:
        src_coords = src_human['coords']
        src_id =  src_human['id']
        po = np.array([[[src_coords[0]*scale_up,src_coords[1]*scale_up+10]]],dtype='float32')
        p_out = cv2.perspectiveTransform(po, H)
        x_out = int(p_out[0][0][0]/scale_down)
        y_out = int(p_out[0][0][1]/scale_down)
        cv2.circle(dst_img, (x_out,y_out), tresh, Color()[src_id], thickness=4, lineType=8, shift=0)
        min_dist = 99999
        min_dist_human = None
        for c2_human in dst_poses:
            c2_coords = c2_human['coords']
            d = distance.euclidean((x_out,y_out),c2_coords)

            if d < min_dist and d < tresh:
                min_dist = d
                min_dist_human = c2_human

        if min_dist_human:
            cv2.line(dst_img, (x_out,y_out), min_dist_human['coords'], Color()[src_id],thickness=5)
            pass

    return dst_img

def process_closest(src_frames,dst_frames,H,scale_up=1,scale_down=1):
    '''
    id_match_counts = {
        '42': {'32': count_of_matches}
    }
    '''
    id_match_counts = {}
    for src_poses,dst_poses in zip(src_frames,dst_frames):
        for src_human in src_poses:
            src_coords = src_human['coords']
            src_id =  src_human['id']
            po = np.array([[[src_coords[0]*scale_up,src_coords[1]*scale_up+10]]],dtype='float32')
            p_out = cv2.perspectiveTransform(po, H)
            x_out = int(p_out[0][0][0]/scale_down)
            y_out = int(p_out[0][0][1]/scale_down)

            min_dist = 99999
            min_dist_human = None
            for c2_human in dst_poses:
                c2_coords = c2_human['coords']
                d = distance.euclidean((x_out,y_out),c2_coords)

                if d < min_dist and d < tresh:
                    min_dist = d
                    min_dist_human = c2_human

            if min_dist_human:
                if src_id in id_match_counts:
                    if min_dist_human['id'] in id_match_counts[src_id]:
                        id_match_counts[src_id][min_dist_human['id']] = id_match_counts[src_id][min_dist_human['id']] + 1
                    else:
                        id_match_counts[src_id][min_dist_human['id']] = 1
                else:
                    id_match_counts[src_id] = {min_dist_human['id']:1}

    matches = {}
    for src_id in id_match_counts:
        max_count = 0
        max_count_id = None
        for dst_id in id_match_counts[src_id]:
            if id_match_counts[src_id][dst_id] > max_count:
                max_count = id_match_counts[src_id][dst_id]
                max_count_id = dst_id
        if max_count_id:
            matches[src_id] = max_count_id
            matches[max_count_id] = src_id

    return matches


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise

def get_hip(pose):
    try:
        return (pose.body_parts[8].x, pose.body_parts[8].y)
    except Exception as e:
        pass
    try:
        return (pose.body_parts[11].x, pose.body_parts[11].y)
    except Exception as e:
        pass
    raise Exception("Nohip")


def pose_to_vector(pose):
    neck = (pose.body_parts[1].x, pose.body_parts[1].y)
    neck = np.array([neck[0],neck[1]],dtype='float32')
    hip = get_hip(pose)
    hip = np.array([hip[0],hip[1]],dtype='float32')
    body_norm = np.linalg.norm(neck-hip)
    ret = []

    for i in range(common.CocoPart.Background.value):
        if i not in pose.body_parts.keys():
            ret.append([0,0])
            continue

        vec = np.array([pose.body_parts[i].x,pose.body_parts[i].y],dtype='float32')
        vec = vec-neck
        vec = vec/body_norm
        #vec = (vec * 1000)+np.array([100,100])
        ret.append(vec.tolist())

    return ret


def extract_and_save_bounding_boxes(src_img,poses,frame_no,cam_id = "default",match = None):
    for pose_data in poses:
        id = pose_data['id']
        if match:
            try:
                matched_id = match[id]
            except Exception as e:
                continue
        else:
            matched_id = id
        pose = pose_data['pose']

        down = get_most_down_point(pose)
        top = get_most_top_point(pose)
        right = get_most_right_point(pose)
        left = get_most_down_point(pose)
        height = down[1] - top[1]
        offset = height*0.1
        width = (height+(2*offset))/4

        neck = get_neck(pose)
        x1 = int(neck[0]-width)
        y1 = int(top[1]-offset)

        x2 = int(neck[0]+width)
        y2 = int(down[1] + offset)

        if x1 < 0 or x2 >= image_w or y1 < 0 or y2 >= image_h:
            continue
        cropped = src_img[y1:y2, x1:x2]
        if args.save:

            mkdir_p(args.save.rstrip("/") + "/" + str(matched_id) + "/" + cam_id + "/" + str(id) + "/")
            try:
                pickle.dump(pose_to_vector(pose),open(args.save.rstrip("/") + "/" + str(matched_id) + "/" + cam_id + "/" + str(id) + "/" + str(frame_no) +".pose",'wb'))
                cv2.imwrite(args.save.rstrip("/") + "/" + str(matched_id) + "/" + cam_id + "/" + str(id) + "/" + str(frame_no) +".jpg" ,cropped)
            except Exception as e:
                pass
        else:
            cv2.rectangle(src_img,(x1,y1),(x2,y2),(255,255,255),thickness=3)

        #cv2.imshow("bounding",cropped)
    return src_img


matches_entry_to_overview = process_closest(entry_tracks, overview_tracks,homography_entry_to_overview,1,2)
matches_d105_to_overview = process_closest(d105_tracks, overview_tracks,homography_d105_to_overview,2,2)
matches_e112_to_overview = process_closest(e112_tracks, overview_tracks,homography_e112_to_overview,2,2)


for i,(overview_poses,entry_poses,d105_poses,e112_poses) in enumerate(zip(overview_tracks,entry_tracks,d105_tracks,e112_tracks)):
    if cap_overview.isOpened() and cap_entry.isOpened() and cap_d105.isOpened() and cap_e112.isOpened():
        _, im_overview = cap_overview.read()
        _, im_entry = cap_entry.read()
        _, im_d105 = cap_d105.read()
        _, im_e112 = cap_e112.read()

        if args.ui:

            #im_overview = draw_to_image_mapped(entry_poses,homography_entry_to_overview,im_overview,1,2)
            #im_overview = draw_to_image_mapped(d105_poses,homography_d105_to_overview,im_overview,2,2)
            #im_overview = draw_to_image_mapped(e112_poses,homography_e112_to_overview,im_overview,2,2)


            im_overview = draw_poses_to_image(im_overview, overview_poses)
            im_entry = draw_poses_to_image(im_entry, entry_poses)
            im_d105 = draw_poses_to_image(im_d105, d105_poses)
            im_e112 = draw_poses_to_image(im_e112, e112_poses)

            im_overview = draw_paired_poses_to_image(im_overview, overview_poses)
            im_entry = draw_paired_poses_to_image(im_entry, entry_poses,matches_entry_to_overview)
            im_d105 = draw_paired_poses_to_image(im_d105, d105_poses,matches_d105_to_overview)
            im_e112 = draw_paired_poses_to_image(im_e112, e112_poses,matches_e112_to_overview)

            #im_overview = draw_closest(entry_poses, overview_poses, im_overview, homography_entry_to_overview,1,2)
            #im_overview = draw_closest(d105_poses, overview_poses, im_overview, homography_d105_to_overview,2,2)
            im_overview = draw_closest(e112_poses, overview_poses, im_overview, homography_e112_to_overview,2,2)

        #im_overview = extract_and_save_bounding_boxes(im_overview, overview_poses, i, "overview")
        #im_entry = extract_and_save_bounding_boxes(im_entry, entry_poses, i, args.entry.split("/")[-1].split(".")[0], matches_entry_to_overview)
        #im_d105 = extract_and_save_bounding_boxes(im_d105, d105_poses, i, args.d105.split("/")[-1].split(".")[0], matches_d105_to_overview)
        #im_e112 = extract_and_save_bounding_boxes(im_e112, e112_poses, i, args.e112.split("/")[-1].split(".")[0], matches_e112_to_overview)
        print("Frame:", i)
        if args.ui:
            #=============VISUALISE===========================
            im_overview =  cv2.resize(im_overview,(768,432))
            im_entry =  cv2.resize(im_entry,(768,432))
            im_d105 =  cv2.resize(im_d105,(768,432))
            im_e112 =  cv2.resize(im_e112,(768,432))

            #r1 = np.concatenate((im_overview, im_entry), axis=1)
            #r2 = np.concatenate((im_d105,im_e112), axis=1)
            #screen = np.concatenate((r1,r2), axis=0)
            screen = np.concatenate((im_overview,im_e112), axis=0)

            cv2.imshow('Numpy Horizontal Concat', screen)

            cv2.waitKey(25)

exit(0)
