# -*- coding: utf-8 -*-
'''
This Program stitches two images based on user input correspondences
Author: Tony Joseph
'''

import numpy as np
import cv2
import matplotlib.pyplot as plt
import sys
import os
import argparse





def getImageStitch(image1, image2, H):
    new_resolution = (int(image1.shape[1] + image2.shape[1] * 1.5), int(image1.shape[0]*1.5))
    stitch_image1 = cv2.warpPerspective(image2, H, new_resolution) / 100
    stitch_image2 = cv2.warpPerspective(image2, H, new_resolution) / 1
    stitch_image3 = cv2.warpPerspective(image2, H, new_resolution) / 2
    stitch_image1[0:image1.shape[0], 0:image1.shape[1]] += image1 / 1
    stitch_image2[0:image1.shape[0], 0:image1.shape[1]] += image1 / 100
    stitch_image3[0:image1.shape[0], 0:image1.shape[1]] += image1 / 2

    return (stitch_image1, stitch_image2, stitch_image3,)



image1 =  cv2.imread('S1000001_img_3040.png')/255
image2 =  cv2.imread('S1090001_img_6011.png')/255
print(image1.shape)
#image1 = cv2.resize(image1, (1000, 600))
#image2 = cv2.resize(image2, (1000, 600))




H = np.load('S1000001_img_3040.png_to_S1090001_img_6011.png.npy')
print(H)
stitched_images = getImageStitch(image1, image2, H)
stitched_images = [cv2.resize(img, (0, 0), fx=0.25, fy=0.25) for img in stitched_images]

i = 0
while True:
    cv2.imshow('output', stitched_images[i])
    if cv2.waitKey() == 27:
        break;
    i = (i + 1) % len(stitched_images)

cv2.imwrite('output.jpg', stitched_image)
# Display the Output Image:
cv2.namedWindow("Stitched_image")
cv2.moveWindow("Stitched_image", 300,250)
cv2.imshow("Stitched_image", stitched_image)
cv2.waitKey(0)
