import cv2
import numpy as np
import argparse


def getImageStitch(image1, image2, H):
    new_resolution = (int(image1.shape[1] + image2.shape[1] * 1.5), int(image1.shape[0]*1.5))
    stitch_image1 = cv2.warpPerspective(image2, H, new_resolution) / 100
    stitch_image2 = cv2.warpPerspective(image2, H, new_resolution) / 1
    stitch_image3 = cv2.warpPerspective(image2, H, new_resolution) / 2
    stitch_image1[0:image1.shape[0], 0:image1.shape[1]] += image1 / 1
    stitch_image2[0:image1.shape[0], 0:image1.shape[1]] += image1 / 100
    stitch_image3[0:image1.shape[0], 0:image1.shape[1]] += image1 / 2

    return (stitch_image1, stitch_image2, stitch_image3,)

resize = (1280,720)

parser = argparse.ArgumentParser(description='tf-pose-estimation run')
parser.add_argument('--src', type=str)
parser.add_argument('--dst', type=str)
parser.add_argument('--com', action="store_true")
parser.add_argument('--dump', type=str)

args = parser.parse_args()

_,dst = cv2.VideoCapture(args.dst).read()
_,src = cv2.VideoCapture(args.src).read()

dst = dst/255
src = src/255

if not args.com:

    cv2.namedWindow('src')
    cv2.namedWindow('dst')

    points = []
    image = dst
    def draw_circle(event,x,y,flags,param):
        if event == cv2.EVENT_LBUTTONDOWN:
            x = x*3
            y = y*3
            cv2.circle(image,(x,y),3,(255,255,255),-1)
            points.append([x,y])
            print(x,y)

    cv2.setMouseCallback('dst',draw_circle)

    cv2.imshow('src',cv2.resize(src,resize))
    cv2.imshow('dst',cv2.resize(dst,resize))

    while(1):
        cv2.imshow('dst',cv2.resize(dst,resize))
        if cv2.waitKey(20) & 0xFF == 27:
            break

    dst_p = points[:]
    points = []
    image = src

    cv2.setMouseCallback('dst',lambda *args : None)
    cv2.setMouseCallback('src',draw_circle)

    while(1):
        cv2.imshow('src',cv2.resize(src,resize))
        if cv2.waitKey(20) & 0xFF == 27:
            break

    src_p = points[:]

    cv2.destroyAllWindows()

if args.com:
    dst_p = [[946, 1066],[1898, 1208],[716, 1598],[634, 1608]]

    src_p = [[920, 1062],[1776, 1106],[1444, 1422],[1394, 1432]]


print(dst_p)
print(src_p)
# Calculate Homography
h, status = cv2.findHomography(np.array(dst_p), np.array(src_p))

stitched_images = getImageStitch(src,dst,h)

i = 0
while True:
    cv2.imshow('output', cv2.resize(stitched_images[i],resize))
    if cv2.waitKey(0) == 27:
        break
    i = (i + 1) % len(stitched_images)

h.dump(args.dump)
