# libraries
import numpy as np
import matplotlib.pyplot as plt

# Fake dataset
height = [87.2, 74.0, 72.4, 93.3, 93.3]
bars = ('Postava', 'Obličej', 'Chůze', 'Fúze\n(p + o + ch)', 'Fúze\n(p + ch)')
y_pos = np.arange(len(bars))

fig = plt.figure()


# Create bars and choose color
plt.bar(y_pos, height, color = "green")


for i in range(5):
    plt.text(x = i-0.325 , y = height[i]-7, s = str(height[i]) +"%" , size = 14,color="white")

# Add title and axis names
plt.title('EER výsledky modalit')
#plt.xlabel('categories')
plt.ylabel('EER ACC (%)')

# Limits for the Y axis
plt.ylim(0,100)

# Create names
plt.xticks(y_pos, bars)

# Show graphic
plt.show()

fig.savefig("scores.pdf")
