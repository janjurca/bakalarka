
import numpy as np

import argparse
import dlib
import glob
import json
import os

def main():
    # Load all the models we need: a detector to find the faces, a shape predictor
    # to find face landmarks so we can precisely localize the face, and finally the
    # face recognition model.
    detector = dlib.get_frontal_face_detector()
    sp = dlib.shape_predictor(args.predictor)
    facerec = dlib.face_recognition_model_v1(args.model)
    faces = {}
    files = glob.glob(args.src.rstrip("/") + "/*/*/*.jpg")
    count = len(files)
    print (count)
    for i,file in enumerate(files):
        if args.start:
            if i < args.start:
                print("Skipped",i,"/",count,file)
                continue

        print(i,"/",count,file)
        img = dlib.load_rgb_image(file)

        #win = dlib.image_window()
        dets = detector(img, 1)
        if len(dets) >= 1:
            face = dets[0]


            dir = args.dest +  file.rsplit("/",1)[0]
            if not os.path.isdir(dir):
                os.makedirs(dir)

            img = img[face.top():face.bottom(),face.left():face.right()]
            dlib.save_image(img,dir +"/" + file.rsplit("/",1)[1])

        if args.stop:
            if i >= args.stop:
                break

    return

if __name__ == '__main__':

    example_usage = '''Call this program like this:\n
       ./face_recognition.py shape_predictor_5_face_landmarks.dat dlib_face_recognition_resnet_model_v1.dat ../examples/faces\n
    You can download a trained facial shape predictor and recognition model from:\n
        http://dlib.net/files/shape_predictor_5_face_landmarks.dat.bz2\n
        http://dlib.net/files/dlib_face_recognition_resnet_model_v1.dat.bz2'''

    parser = argparse.ArgumentParser(example_usage)
    parser.add_argument('-p', '--predictor', action='store', help='predictor .dat file', default='')
    parser.add_argument('-m', '--model', action='store', help='model .dat file', default='config.json')
    parser.add_argument("-v", "--src", action="store",default="", type=str, help="src path")
    parser.add_argument("-d", "--dest", action="store",default="", type=str, help="dest path")
    parser.add_argument("-s", "--start", action="store",default=None, type=int, help="first index")
    parser.add_argument("-c", "--stop", action="store",default=None, type=int, help="last index")
    args = parser.parse_args()
    main()
