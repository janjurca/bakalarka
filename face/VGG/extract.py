from keras.models import Model, Sequential
from keras.layers import Input, Convolution2D, ZeroPadding2D, MaxPooling2D, Flatten, Dense, Dropout, Activation
import numpy as np
from keras.preprocessing.image import load_img, img_to_array
from keras.applications.imagenet_utils import preprocess_input
from keras.preprocessing import image
import argparse
import glob
import json
import os
import pickle
def main():

    model = Sequential()
    model.add(ZeroPadding2D((1,1),input_shape=(224,224, 3)))
    model.add(Convolution2D(64, (3, 3), activation='relu'))
    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(64, (3, 3), activation='relu'))
    model.add(MaxPooling2D((2,2), strides=(2,2)))

    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(128, (3, 3), activation='relu'))
    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(128, (3, 3), activation='relu'))
    model.add(MaxPooling2D((2,2), strides=(2,2)))

    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(256, (3, 3), activation='relu'))
    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(256, (3, 3), activation='relu'))
    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(256, (3, 3), activation='relu'))
    model.add(MaxPooling2D((2,2), strides=(2,2)))

    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(512, (3, 3), activation='relu'))
    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(512, (3, 3), activation='relu'))
    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(512, (3, 3), activation='relu'))
    model.add(MaxPooling2D((2,2), strides=(2,2)))

    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(512, (3, 3), activation='relu'))
    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(512, (3, 3), activation='relu'))
    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(512, (3, 3), activation='relu'))
    model.add(MaxPooling2D((2,2), strides=(2,2)))

    model.add(Convolution2D(4096, (7, 7), activation='relu'))
    model.add(Dropout(0.5))
    model.add(Convolution2D(4096, (1, 1), activation='relu'))
    model.add(Dropout(0.5))
    model.add(Convolution2D(2622, (1, 1)))
    model.add(Flatten())
    model.add(Activation('softmax'))


    from keras.models import model_from_json
    model.load_weights(args.model)

    def preprocess_image(image_path):
        img = load_img(image_path, target_size=(224, 224))
        img = img_to_array(img)
        img = np.expand_dims(img, axis=0)
        img = preprocess_input(img)
        return img

    def findCosineSimilarity(source_representation, test_representation):
        a = np.matmul(np.transpose(source_representation), test_representation)
        b = np.sum(np.multiply(source_representation, source_representation))
        c = np.sum(np.multiply(test_representation, test_representation))
        return 1 - (a / (np.sqrt(b) * np.sqrt(c)))

    def findEuclideanDistance(source_representation, test_representation):
        euclidean_distance = source_representation - test_representation
        euclidean_distance = np.sum(np.multiply(euclidean_distance, euclidean_distance))
        euclidean_distance = np.sqrt(euclidean_distance)
        return euclidean_distance

    vgg_face_descriptor = Model(inputs=model.layers[0].input, outputs=model.layers[-2].output)


    epsilon = 0.40

    def verifyFace(img1, img2):
        img1_representation = vgg_face_descriptor.predict(preprocess_image('C:/Users/IS96273/Desktop/trainset/%s' % (img1)))[0,:]
        img2_representation = vgg_face_descriptor.predict(preprocess_image('C:/Users/IS96273/Desktop/trainset/%s' % (img2)))[0,:]

        cosine_similarity = findCosineSimilarity(img1_representation, img2_representation)
        euclidean_distance = findEuclideanDistance(img1_representation, img2_representation)

        print("Cosine similarity: ",cosine_similarity)
        print("Euclidean distance: ",euclidean_distance)

        if(cosine_similarity < epsilon):
            print("verified... they are same person")
        else:
            print("unverified! they are not same person!")

        f = plt.figure()
        f.add_subplot(1,2, 1)
        plt.imshow(image.load_img('C:/Users/IS96273/Desktop/trainset/%s' % (img1)))
        plt.xticks([]); plt.yticks([])
        f.add_subplot(1,2, 2)
        plt.imshow(image.load_img('C:/Users/IS96273/Desktop/trainset/%s' % (img2)))
        plt.xticks([]); plt.yticks([])
        plt.show(block=True)
        print("-----------------------------------------")


    def get_descriptor(img):
        return vgg_face_descriptor.predict(img)[0,:]



    faces = {}
    files = glob.glob(args.src.rstrip("/") + "/" + "/*/*/*.jpg")
    count = len(files)
    print (count)
    for i,file in enumerate(files):
        if args.start:
            if i < args.start:
                print("Skipped",i,"/",count,file)
                continue

        l = file.split("/")
        if not l[1] in faces:
            faces[l[1]] = dict()
        if not l[2] in faces[l[1]]:
            faces[l[1]][l[2]] = dict()

        print(i,"/",count,file)

        img = preprocess_image(file)
        face_descriptor = get_descriptor(img)
        desc = face_descriptor
        faces[l[1]][l[2]][file] = desc

        if i%1000 == 0 :
            pickle.dump(faces,open(args.file,'wb'))
        if args.stop:
            if i >= args.stop:
                break

    pickle.dump(faces,open(args.file,'wb'))


if __name__ == '__main__':

    example_usage = '''Call this program like this:\n
       ./face_recognition.py shape_predictor_5_face_landmarks.dat dlib_face_recognition_resnet_model_v1.dat ../examples/faces\n
    You can download a trained facial shape predictor and recognition model from:\n
        http://dlib.net/files/shape_predictor_5_face_landmarks.dat.bz2\n
        http://dlib.net/files/dlib_face_recognition_resnet_model_v1.dat.bz2'''

    parser = argparse.ArgumentParser(example_usage)
    parser.add_argument("-f", "--file",  action="store", type=str, help="pickle target")
    parser.add_argument("-v", "--src", action="store",default="", type=str, help="src folder")
    parser.add_argument("-m", "--model", action="store",default="/home/rmin/Dokumenty/bakalarka/vgg_face_weights.h5", type=str, help="model file")
    parser.add_argument("-s", "--start", action="store",default=None, type=int, help="first index")
    parser.add_argument("-c", "--stop", action="store",default=None, type=int, help="last index")
    args = parser.parse_args()
    main()
