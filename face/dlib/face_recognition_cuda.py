#!/usr/bin/python
import sys
import os
import dlib
import glob
import cv2
import pickle
from time import sleep
import argparse
import numpy

def main():
    # Load all the models we need: a detector to find the faces, a shape predictor
    # to find face landmarks so we can precisely localize the face, and finally the
    # face recognition model.
    detector = dlib.get_frontal_face_detector()
    sp = dlib.shape_predictor(args.predictor)
    facerec = dlib.face_recognition_model_v1(args.model)
    faces = {}
    files = glob.glob(args.glob)
    count = len(files)
    for i,file in enumerate(files):
        print(i,"/",count,file,end='', flush=True)
        img = None
        try:
            img = dlib.load_rgb_image(file)
        except Exception as e:
            print("Failed to load image",str(e))
            continue

        dets = detector(img, 2)
        if len(dets) >= 1:
            face = dets[0]
            shape = sp(img, face)

            #win.clear_overlay()
            #win.add_overlay(face)
            #win.add_overlay(shape)

            face_descriptor = facerec.compute_face_descriptor(img, shape)
            desc = numpy.zeros(shape=128)
            for k in range(0, len(face_descriptor)):
                desc[k] = face_descriptor[k]

            faces[file] = desc
            print("ANO")
        else:
            print("")

        if i % 10000 == 0:
            pickle.dump(faces,open(args.file,'wb'))

    pickle.dump(faces,open(args.file,'wb'))

example_usage = '''Call this program like this:\n
   ./face_recognition.py shape_predictor_5_face_landmarks.dat dlib_face_recognition_resnet_model_v1.dat ../examples/faces\n
You can download a trained facial shape predictor and recognition model from:\n
    http://dlib.net/files/shape_predictor_5_face_landmarks.dat.bz2\n
    http://dlib.net/files/dlib_face_recognition_resnet_model_v1.dat.bz2'''

parser = argparse.ArgumentParser(example_usage)
parser.add_argument('-p', '--predictor', action='store', help='predictor .dat file', default='shape_predictor_5_face_landmarks.dat')
parser.add_argument('-m', '--model', action='store', help='model .dat file', default='dlib_face_recognition_resnet_model_v1.dat')
parser.add_argument("-f", "--file",  action="store", type=str, help="pickle target")
parser.add_argument("-g", "--glob", action="store",default=None, type=str, help="source folder glob")
args = parser.parse_args()
main()
