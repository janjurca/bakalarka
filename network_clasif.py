import torch
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms
import torch.nn.functional as F
import torch
import torch.nn
import random
from torch.utils.data import DataLoader,Dataset
import pickle
import numpy as np
from torch import optim
import matplotlib.pyplot as plt

# Device configuration
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

def dprint(*args, **kwargs):
    #print( " ".join(map(str,args)), **kwargs)
    pass

class NetworkDataset(Dataset):
    def __init__(self,file="final.pic"):
        self.data = pickle.load(open(file,'rb'))
        l = 0
        for person in self.data:
            for track in person:
                l = l + 1
        self.l = l

    def __getitem__(self,index):
        l = 0
        for i, person in enumerate(self.data):
            for track in person:
                if l == index:
                    return track, i
                l = l + 1
        return None

    def __len__(self):
        return self.l

# Fully connected neural network with one hidden layer
class NeuralNet(nn.Module):
    def __init__(self):
        super(NeuralNet, self).__init__()
        # kernel

        self.weight0 = torch.ones((1,128))

        self.q0 = torch.nn.Parameter(self.weight0)
        self.fc1 = nn.Linear(128, 128)
        self.fc2 = nn.Linear(128, 128)

    def forward(self, X):
        dprint("Q0:",self.q0)
        dprint("Input X:", X)
        o = torch.matmul(self.q0, X)
        dprint("Vahy O:",o)
        o = F.softmax(o, dim=1)
        dprint("Softmax vah",o)

        o = torch.tanh(self.fc1((X*o).mean(dim=-1)))
        o = torch.matmul(o, X)
        o = F.softmax(o)
        o = self.fc2((X*o).mean(dim=-1))
        dprint("Final:",o)
        return o



dataset = NetworkDataset("/home/rmin/Dokumenty/extracted/filtered.pic")


net = NeuralNet().to(device)
criterion = nn.CrossEntropyLoss()
optimizer = optim.Adam(net.parameters(),lr = 0.00001 )



counter = []
loss_history = []
iteration_number= 0

for epoch in range(0,20):
    batch = []
    for i in range(len(dataset)):
        batch.append(dataset[i])

    for i, data in enumerate(batch):
        t0 , label = data
        output = net(t0.float())
        print(output)
        exit(0)
        loss = criterion(output,label)
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

    print("Epoch number {}\n Current loss {}\n".format(epoch,loss.item()))
    iteration_number += 1
    counter.append(iteration_number)
    loss_history.append(loss.item())
#show_plot(counter,loss_history)
plt.plot(loss_history)
plt.show()
