import matplotlib
from matplotlib import pyplot as plt
import argparse
import os
import shutil
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.autograd import Variable
import torch.backends.cudnn as cudnn
from tripletnet import Tripletnet
from scipy.optimize import brentq
from scipy.interpolate import interp1d
from sklearn.metrics import roc_curve

import numpy as np
from tripletDataset import *
from network import *

from sklearn import metrics
from sklearn.metrics import roc_curve, auc

import argparse

def static_vars(**kwargs):
    def decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func
    return decorate

@static_vars(tc=1)
def plotroc(fpr,tpr):
    plt.clf()
    plt.plot(fpr, tpr, lw=2, label="net")
    plt.legend(loc="lower right")
    plt.pause(0.05)
    #plt.savefig("figure" + str(plotroc.tc) + ".jpg")
    plotroc.tc = plotroc.tc + 1


def eer_test(test_loader, tnet, tresh):
    good = 0
    all = 0
    for batch_idx, (anchor,positive,negative) in enumerate(test_loader):
        # compute output
        dista, distb, em1, em2, em3 = tnet(anchor,positive,negative)
        test_loss =  criterion(em1, em2, em3).data[0]
        for da,db in zip(dista,distb):
            if da.item() < tresh:
                good = good + 1
            all = all + 1

            if db.item() >= tresh:
                good = good + 1
            all = all + 1

    print("ALL",all,"GOOD",good,"SCORE",good/all)

@static_vars(tpr={})
@static_vars(fpr={})
@static_vars(tc=1)
def test(test_loader, tnet, criterion):
    # switch to evaluation mode
    tnet.eval()
    for batch_idx, (anchor,positive,negative) in enumerate(test_loader):
        # compute output
        dista, distb, em1, em2, em3 = tnet(anchor,positive,negative)
        test_loss =  criterion(em1, em2, em3).data[0]

        pos_labels = torch.tensor(np.full((1, len(distb)), 1).squeeze())
        neg_labels = torch.tensor(np.full((1, len(dista)), 0).squeeze())

        labels = torch.cat((pos_labels,neg_labels),dim=0)
        dists = torch.cat((distb,dista),dim=0)

        fpr, tpr, thresholds = roc_curve(np.array(labels.tolist()),np.array(dists.tolist()))
        roc_auc = auc(fpr, tpr)

        if args.fpr:
            r = {
            "fpr":fpr,
            "tpr":tpr,
            "auc":roc_auc
            }
            print("Ukladam")
            pickle.dump(r,open(args.fpr,'wb'))


        eer = brentq(lambda x : 1. - x - interp1d(fpr, tpr)(x), 0., 1.)
        thresh = interp1d(fpr, thresholds)(eer)
        print("TRESH",thresh)
        plotroc(fpr,tpr)
        print("ROC",roc_auc)
        print('Test set: loss: {:.4f}, AUC: {:.2f}%'.format(test_loss, roc_auc))
    eer_test(test_loader,tnet,thresh)
    return roc_auc


if __name__ == '__main__':
    plt.figure()
    plt.show(False)
    plt.draw()
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic example')
    plt.legend(loc="lower right")

    parser = argparse.ArgumentParser()
    parser.add_argument("--test", action="store",default="/home/rmin/Dokumenty/extracted/agregated/fusion.agregated.triplets", type=str, help="test triplets file")
    parser.add_argument("--weights", action="store",type=str, help="load torch")
    parser.add_argument("--batch", action="store",type=int,default=500, help="load torch")
    parser.add_argument("--pg", action="store_true", help="gait person")
    parser.add_argument("--model", action="store",required=True, type=str, help="model to use")
    parser.add_argument("-f", "--fpr", action="store", type=str, help="source file")

    args = parser.parse_args()

    if "pg" in args.model:
        test_loader = torch.utils.data.DataLoader(
            TripletNetworkDatasetUnifiedPG(data=args.test,drop=False),
            batch_size=args.batch, shuffle=False)
    else:
        test_loader = torch.utils.data.DataLoader(
            TripletNetworkDatasetUnified(data=args.test,drop=False),
            batch_size=args.batch, shuffle=False)


    models = {
    "cat": FusionCat(),
    "sum": FusionSum(),
    "simple": FusionSimple(),
    "pgcat": PGFusionCat(),
    "pgsum": PGFusionSum(),
    "pgsimple": PGFusionSimple(),
    }

    model = models[args.model]
    tnet = Tripletnet(model)

    criterion = nn.TripletMarginLoss(margin=1.0, p=2)
    optimizer = optim.Adam(tnet.parameters(), lr=0.001)

    if args.weights:
        load_checkpoint(tnet, optimizer, args.weights)

    n_parameters = sum([p.data.nelement() for p in tnet.parameters()])
    print('  + Number of params: {}'.format(n_parameters))

    test(test_loader, tnet, criterion)
