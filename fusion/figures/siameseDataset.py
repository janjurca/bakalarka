
import pickle
import torch
from torch.utils.data import DataLoader,Dataset
import numpy as np
import torch.utils.data

class SiameseNetworkDataset(Dataset):
    def __init__(self,dlib="/home/rmin/Dokumenty/extracted/agregated/vecs.dlib.pic",VGG="/home/rmin/Dokumenty/extracted/agregated/vecs.vgg.pic",splits="/home/rmin/Dokumenty/extracted/agregated/splits.txt"):
        self.dlib = pickle.load(open(dlib,'rb'))
        self.vgg = pickle.load(open(VGG,'rb'))
        self.indexex = []
        with open(splits,'r') as f:
            for line in f.read().splitlines():
                s = line.split(",")
                p1 = s[2].split("/")[0].strip()
                t1 = s[2].split("/")[1].strip()

                p2 = s[3].split("/")[0].strip()
                t2 = s[3].split("/")[1].strip()

                label = int(s[-1].strip())
                self.indexex.append((p1,t1,p2,t2,label))



    def __getitem__(self,index):
        i = self.indexex[index]
        #print(i,int(i[4]))
        dlib_vec1 = self.dlib[i[0]][i[1]]
        vgg_vec1 = self.vgg[i[0]][i[1]]

        dlib_vec2 = self.dlib[i[2]][i[3]]
        vgg_vec2 = self.vgg[i[2]][i[3]]

        label = 1 if int(i[4]) == 1 else -1
        #print((dlib_vec1, vgg_vec1), (dlib_vec2, vgg_vec2), i[4])
        return (dlib_vec1, vgg_vec1), (dlib_vec2, vgg_vec2), label

    def __len__(self):
        return len(self.indexex)




class SiameseNetworkDatasetUnified(Dataset):
    def __init__(self,data="tensors_vgg_dlib.pic"):
        self.data = pickle.load(open(data,'rb'))
        self.indexex = []


    def __getitem__(self,index):
        tensor_dlib_vecs1 = self.data["tensor_dlib_vecs1"][index]
        tensor_dlib_vecs2 = self.data["tensor_dlib_vecs2"][index]
        tensor_vgg_vecs1 = self.data["tensor_vgg_vecs1"][index]
        tensor_vgg_vecs2 = self.data["tensor_vgg_vecs2"][index]
        tensor_labels = self.data["tensor_labels"][index]

        return (tensor_dlib_vecs1, tensor_vgg_vecs1), (tensor_dlib_vecs2, tensor_vgg_vecs2), tensor_labels

    def __len__(self):
        return len(self.data["tensor_labels"])
