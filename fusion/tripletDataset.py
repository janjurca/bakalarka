
import pickle
import torch
from torch.utils.data import DataLoader,Dataset
import numpy as np
import torch.utils.data
import random

class TripletNetworkDatasetUnified(Dataset):
    def __init__(self,data="/home/rmin/Dokumenty/extracted/agregated/fusion.agregated.triplets",drop=False):
        self.data = pickle.load(open(data,'rb'))
        self.drop = drop

    def __getitem__(self,index):
        (anchor_gait,anchor_face,anchor_person), (positive_gait,positive_face,positive_person), (negative_gait,negative_face,negative_person) = self.data[index]
        anchor_face,anchor_person,anchor_gait = torch.tensor(anchor_face).float(), torch.tensor(anchor_person).float(),torch.tensor(anchor_gait).float()
        positive_face,positive_person,positive_gait = torch.tensor(positive_face).float(), torch.tensor(positive_person).float(),torch.tensor(positive_gait).float()
        negative_face,negative_person,negative_gait = torch.tensor(negative_face).float(), torch.tensor(negative_person).float(),torch.tensor(negative_gait).float()

        anchor_person = anchor_person.squeeze()
        positive_person = positive_person.squeeze()
        negative_person = negative_person.squeeze()


        anchor = (anchor_face,anchor_person,anchor_gait)
        positive = (positive_face,positive_person,positive_gait)
        negative = (negative_face,negative_person,negative_gait)
        return anchor, positive, negative

    def __len__(self):
        return len(self.data)



class TripletNetworkDatasetUnifiedPG(Dataset):
    def __init__(self,data="/home/rmin/Dokumenty/extracted/agregated/fusion.agregated.triplets",drop=False):
        self.data = pickle.load(open(data,'rb'))
        self.drop = drop

    def __getitem__(self,index):
        (anchor_gait,anchor_person), (positive_gait,positive_person), (negative_gait,negative_person) = self.data[index]
        anchor_person,anchor_gait = torch.tensor(anchor_person).float(), torch.tensor(anchor_gait).float()
        positive_person,positive_gait = torch.tensor(positive_person).float(),torch.tensor(positive_gait).float()
        negative_person,negative_gait = torch.tensor(negative_person).float(),torch.tensor(negative_gait).float()


        anchor = (anchor_person,anchor_gait)
        positive = (positive_person,positive_gait)
        negative = (negative_person,negative_gait)
        return anchor, positive, negative

    def __len__(self):
        return len(self.data)
