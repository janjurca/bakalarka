import torch
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms
import torch.nn.functional as F
import torch
import torch.nn
import random
from torch.utils.data import DataLoader,Dataset
import pickle
import numpy as np
from torch import optim
import matplotlib.pyplot as plt
import os
import argparse
import torch.nn.functional as F

# Device configuration
#device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

def dprint(*args, **kwargs):
    print( " ".join(map(str,args)), **kwargs)
    pass

class FusionSimple(nn.Module):
    def __init__(self):
        super(FusionSimple, self).__init__()
        self.fuse = nn.Linear(2432,2432)

    def forward(self, vecs):
        face,person,gait = vecs
        o = torch.cat((face,person,gait),dim=1)
        o = self.fuse(o)
        return o


class FusionCat(nn.Module):
    def __init__(self):
        super(FusionCat, self).__init__()
        self.face = nn.Linear(128, 128)
        self.person = nn.Linear(2048, 2048)
        self.gait = nn.Linear(256, 256)

        self.fuse = nn.Linear(2432,128)

    def forward(self, vecs):
        person,gait,face = vecs
        face_multiplier = (self.face(face))
        person_multiplier = (self.person(person))
        gait_multiplier = (self.gait(gait))

        face = face*face_multiplier
        person = person*person_multiplier
        gait = gait*gait_multiplier

        o = torch.cat((face,person,gait),dim=1)
        o = self.fuse(o)
        return o



class FusionSum(nn.Module):
    def __init__(self):
        super(FusionSum, self).__init__()
        self.face = nn.Linear(128, 144)
        self.person = nn.Linear(2048, 144)
        self.gait = nn.Linear(256, 144)

        self.fuse = nn.Linear(2320,2320)

    def forward(self, vecs):
        person,gait,face = vecs
        face_new = (self.face(face))
        person_new = (self.person(person))
        gait_new = (self.gait(gait))
        o = face_new + person_new + gait_new
        return o


class PGFusionSimple(nn.Module):
    def __init__(self):
        super(PGFusionSimple, self).__init__()
        self.fuse = nn.Linear(2304,2304)

    def forward(self, vecs):
        person,gait = vecs
        o = torch.cat((person,gait),dim=1)
        o = self.fuse(o)
        return o


class PGFusionCat(nn.Module):
    def __init__(self):
        super(PGFusionCat, self).__init__()
        self.person = nn.Linear(2048, 2048)
        self.gait = nn.Linear(256, 256)

        self.fuse = nn.Linear(2304,128)

    def forward(self, vecs):
        gait,person = vecs
        person_multiplier = (self.person(person))
        gait_multiplier = (self.gait(gait))

        person = person*person_multiplier
        gait = gait*gait_multiplier

        o = torch.cat((person,gait),dim=1)
        o = self.fuse(o)
        return o



class PGFusionSum(nn.Module):
    def __init__(self):
        super(PGFusionSum, self).__init__()
        self.person = nn.Linear(2048, 144)
        self.gait = nn.Linear(256, 144)

    def forward(self, vecs):
        gait,person = vecs
        person_new = (self.person(person))
        gait_new = (self.gait(gait))
        o = person_new + gait_new
        return o

def euclidean_distance(v1,v2):
    dist = torch.norm(v1 - v2)
    return dist

def agregate_mean(vecs):
    mean = vecs.mean(dim=-2)
    assert len(mean) == 128
    return mean

def agregate_center(vec):
    r =  vec[int(len(vec)/2)]
    assert len(r) == 128
    return r

def load_checkpoint(model, optimizer,filename='checkpoint.pth.tar'):
    # Note: Input model & optimizer should be pre-defined.  This routine only updates their states.
    start_epoch = 0
    if os.path.isfile(filename):
        print("=> loading checkpoint '{}'".format(filename))
        checkpoint = torch.load(filename)
        start_epoch = checkpoint['epoch']
        model.load_state_dict(checkpoint['state_dict'])
        optimizer.load_state_dict(checkpoint['optimizer'])
        print("=> loaded checkpoint '{}' (epoch {})"
                  .format(filename, checkpoint['epoch']))
    else:
        print("=> no checkpoint found at '{}'".format(filename))

    return model, optimizer, start_epoch
